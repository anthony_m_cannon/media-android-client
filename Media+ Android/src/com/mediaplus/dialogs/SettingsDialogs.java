package com.mediaplus.dialogs;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.text.InputType;

import com.mediaplus.main.Constants;

public class SettingsDialogs extends Dialogs {
	private CharSequence[] array;
	private int selected;
	
	public SettingsDialogs(Activity activity) {
		super(activity);
	}

	// Sets & Gets
	public String getSelectedName() {
		return array[selected].toString();
	}
	
	public void setSelected(int selected) {
		this.selected = selected;
	}
	
	// Dialogs
	public void filter(DialogInterface.OnClickListener onClick) {
		array = getFilterArray();
		selected = getCurrentSelected(array, "layout-filter");
		
		onCreateChioceDialog(array, "Select Filter", selected, onClick);
		show();
	}
	
	public void getDeviceNameDialog(String current, OnClickListener onClick) {
		onCreateInputDialog("Enter Device Name", current, InputType.TYPE_CLASS_TEXT, onClick);
		showInput();
	}
	
	public void getServerNameDialog(String current, OnClickListener onClick) {
		onCreateInputDialog("Enter Server Name", current, InputType.TYPE_CLASS_TEXT, onClick);
		showInput();
	}
	
	// private
	private int getCurrentSelected(CharSequence[] array, String key) {
		String current = Constants.pref.getString(key);

		for (int i = 0; i<array.length; i++) {
			if (array[i].equals(current)) {
				return i;
			}
		}

		return 0;
	}
	
	private String[] getFilterArray() {
		return new String[] {"All", "Genre", "Year"};
	}
}
