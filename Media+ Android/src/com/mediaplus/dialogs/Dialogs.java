package com.mediaplus.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnKeyListener;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.mediaplus.R;

public class Dialogs {
	private Activity activity;
	private EditText entry;
	private int selected;
	private Dialog dialog;

	public Dialogs(Activity activity) {
		this.activity = activity;
	}
	
	public EditText getEntry() {
		return entry;
	}
	
	public int getSelected() {
		return selected;
	}
	
	public void setSelected(int selected) {
		this.selected = selected;
	}
	
	public void showInput() {
		show();
		
		dialog.getWindow().clearFlags( WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
	}
	
	public void show() {
		if (dialog != null) {
			dialog.show();
			dialog.setOnKeyListener(onKeyListener);
		}
	}

	public void dismiss() {
		if (dialog != null) {
			hideKeyBoard();
			dialog.dismiss();
			dialog = null;
		}
	}
	
	// Dialogs
	public void onCreateInputDialog(String title, String text, int inputType,
			OnClickListener onClick) {
		AlertDialog.Builder builder = onCreateBasicDialog(title, onClick);
		
		View view = View.inflate(activity.getBaseContext(),
				R.layout.dialog_input, null);

		entry = (EditText) view.findViewById(R.id.entry);
		entry.setInputType(inputType);
		entry.setText(text);
		
		builder.setView(view).setNegativeButton("Cancel", onClick);
		
		dialog = builder.create();
	}
	
	public void onCreateChioceDialog(CharSequence[] array, String title,
			int selected, OnClickListener onClick) {
		AlertDialog.Builder builder = onCreateBasicDialog(title, onClick);

		builder.setSingleChoiceItems(array, selected, onClick)
				.setNegativeButton("Cancel", onClick);

		dialog = builder.create();
	}

	public void onCreateInfoDialog(String title, String message, OnClickListener onClick) {
		AlertDialog.Builder builder = onCreateBasicDialog(title, onClick);

		builder.setMessage(message);

		dialog = builder.create();
	}
	
	// Private
	private Builder onCreateBasicDialog(String title, OnClickListener onClick) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);

		builder.setTitle(title).setPositiveButton("Ok", onClick)
				.setCancelable(false);

		return builder;
	}
	
	OnKeyListener onKeyListener = new OnKeyListener() {
		@Override
		public boolean onKey(DialogInterface arg0, int key, KeyEvent event) {
			if (key == KeyEvent.KEYCODE_BACK) {
				dismiss();
				return true;
			}
			
			return false;
		}
	};
	
	private void hideKeyBoard() {
		
	}
}
