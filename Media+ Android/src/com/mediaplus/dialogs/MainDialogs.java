package com.mediaplus.dialogs;

import android.app.Activity;
import android.content.DialogInterface.OnClickListener;
import android.text.InputType;

public class MainDialogs extends Dialogs {
	public MainDialogs(Activity activity) {
		super(activity);
	}
	
	public void getPinDialog(OnClickListener onClick) {
		onCreateInputDialog("Enter Pin", "", InputType.TYPE_CLASS_NUMBER, onClick);
		showInput();
	}
	
	public void pinErrorDialog() {
		onCreateInfoDialog("Error", "Pin incorrect.", null);
		show();
	}
	
	public void serverErrorDialog() {
		onCreateInfoDialog("Error", "Server not connected.", null);
		show();
	}
	
}
