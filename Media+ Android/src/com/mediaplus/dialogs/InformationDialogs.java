package com.mediaplus.dialogs;

import android.app.Activity;
import android.content.DialogInterface.OnClickListener;
import android.text.InputType;

public class InformationDialogs extends Dialogs {
	public InformationDialogs(Activity activity) {
		super(activity);
	}
	
	public void getPinDialog(OnClickListener onClick) {
		onCreateInputDialog("Enter Pin", "", InputType.TYPE_CLASS_NUMBER, onClick);
		showInput();
	}
}
