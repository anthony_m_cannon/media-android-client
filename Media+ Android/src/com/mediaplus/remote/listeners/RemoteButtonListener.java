package com.mediaplus.remote.listeners;

import android.view.View;
import android.view.View.OnClickListener;

import com.mediaplus.R;
import com.mediaplus.controllers.InformationActivity;
import com.mediaplus.controllers.RemoteActivity;
import com.mediaplus.main.BaseActivity;
import com.mediaplus.main.Constants;
import com.mediaplus.server.handlers.ServerHandler;

public class RemoteButtonListener implements OnClickListener {
	private ServerHandler server;
	
	public RemoteButtonListener() {
		server = Constants.server;
	}
	
	@Override
	public void onClick(View arg0) {
		switch(arg0.getId()) {
		case R.id.infoBtn:
			if (Constants.activity instanceof RemoteActivity) {
				RemoteActivity ra = (RemoteActivity) Constants.activity;
				String itemName = ra.getItemName();
				if (itemName != "" && itemName != null) {
					BaseActivity.launchActivity(InformationActivity.class, ra.getItemName());
				}
			}
			break;
		case R.id.playPauseBtn:
			if (Constants.activity instanceof RemoteActivity) {
				RemoteActivity ra = (RemoteActivity) Constants.activity;
				ra.setPlayingState(!ra.getPlayingState());
				server.media().playPause();
			}
			break;
		case R.id.stopBtn:
			server.media().stop();
			if (Constants.activity instanceof RemoteActivity) {
				Constants.activity.finish();
			}
			break;
		case R.id.muteBtn:
			if (Constants.activity instanceof RemoteActivity) {
				RemoteActivity ra = (RemoteActivity) Constants.activity;

				ra.setMute();
			}
			break;
		}
	}

}
