package com.mediaplus.remote.listeners;

import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.mediaplus.R;
import com.mediaplus.controllers.RemoteActivity;
import com.mediaplus.main.Constants;

public class RemoteSeekListener implements OnSeekBarChangeListener {
	private RemoteActivity ra;
	private boolean volumeChanging, positionChanging;
	
	public RemoteSeekListener() {
		if (Constants.activity instanceof RemoteActivity) {
			ra = (RemoteActivity) Constants.activity;
		}
		
		volumeChanging = false;
		positionChanging = false;
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int sec, boolean arg2) {
		switch(seekBar.getId()) {
		case R.id.volumeBar:
			if (volumeChanging) {
				ra.setVolume(sec);
			}
			break;
		case R.id.positionBar:
			if (positionChanging) {
				int mill = sec * 1000;
				ra.setPosition(mill);
			}
			break;
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		switch(seekBar.getId()) {
		case R.id.volumeBar:
			volumeChanging = true;
			break;
		case R.id.positionBar:
			positionChanging = true;
			break;
		}
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		switch(seekBar.getId()) {
		case R.id.volumeBar:
			volumeChanging = false;
			break;
		case R.id.positionBar:
			positionChanging = false;
			break;
		}
	}
}
