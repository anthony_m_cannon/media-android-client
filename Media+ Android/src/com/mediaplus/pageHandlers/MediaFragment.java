package com.mediaplus.pageHandlers;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mediaplus.R;
import com.mediaplus.controllers.InformationActivity;
import com.mediaplus.main.BaseActivity;
import com.mediaplus.main.Constants;
import com.mediaplus.media.handlers.MediaHandler;

public class MediaFragment extends ListFragment {
	private MediaHandler media;
	private String categoryS;
	private int categoryI;

	public MediaFragment() {

	}

	public MediaFragment(String category, MediaHandler media) {
		this.categoryS = category;
		categoryI = -1;
		this.media = media;
	}

	public MediaFragment(int category, MediaHandler media) {
		this.categoryI = category;
		categoryS = null;
		this.media = media;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);

		ArrayList<?> mediaItems = null;

		ArrayAdapter<String> adapter;
		switch (Constants.mediaType) {
		case IMAGE:
			mediaItems = media.image().getAll();
			break;
		case MUSIC:
			mediaItems = media.music().getAll();
			break;
		case FILM:
			if (categoryS != null) {
				mediaItems = media.film().getByGenre(categoryS);
			} else if (categoryI != -1) {
				mediaItems = media.film().getByYear(categoryI);
			} else {
				mediaItems = media.film().getAll();
			}
			break;
		default:
			if (categoryS != null) {
				mediaItems = media.film().getByGenre(categoryS);
			} else if (categoryI != -1) {
				mediaItems = media.film().getByYear(categoryI);
			} else {
				mediaItems = media.film().getAll();
			}
			break;
		}

		adapter = new MediaListAdapter(inflater.getContext(), mediaItems,
				getListAmount(mediaItems));
		setListAdapter(adapter);

		return view;
	}

	private String[] getListAmount(ArrayList<?> media) {
		if (media == null) {
			return new String[0];
		}

		String[] listAmount = new String[media.size()]; // How many list items

		for (int i = 0; i < listAmount.length; i++) {
			listAmount[i] = "";
		}

		return listAmount;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		position = position - l.getFirstVisiblePosition();
		
		v = l.getChildAt(position);
		
		if (v != null) {
			TextView tv = (TextView) l.getChildAt(position).findViewById(
					R.id.firstLine);
			String itemName = (String) tv.getText();

			BaseActivity.launchActivity(InformationActivity.class, itemName);
		}
		
		super.onListItemClick(l, v, position, id);
	}
}
