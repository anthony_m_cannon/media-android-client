package com.mediaplus.pageHandlers;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.mediaplus.R;
 
public class SettingsListAdapter extends BaseExpandableListAdapter {
    private Activity activity;
    private HashMap<String, ArrayList<String[]>> listCollection;
    private String[] groupList;

	public SettingsListAdapter(Activity activity, String[] groupList,
			HashMap<String, ArrayList<String[]>> listCollection) {

        this.activity = activity;
        this.groupList = groupList;
        this.listCollection = listCollection;
	}
	
	public Object getChild(int groupPosition, int childPosition) {
        return listCollection.get(groupList[groupPosition]).get(childPosition);
    }
	
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }
    
    public View getChildView(final int groupPosition, final int childPosition,
            boolean isLastChild, View convertView, ViewGroup parent) {
    	
        String[] string = (String[]) getChild(groupPosition, childPosition);
        
        if (convertView == null) {
            convertView = View.inflate(activity.getBaseContext(), R.layout.setting_child, null);
        }
        
        TextView setting = (TextView) convertView.findViewById(R.id.setting);
        TextView value = (TextView) convertView.findViewById(R.id.value);
        
        setting.setText(string[0]);
        value.setText(string[1]);
        
        return convertView;
    }
 
    public int getChildrenCount(int groupPosition) {
        return listCollection.get(groupList[groupPosition]).size();
    }
 
    public Object getGroup(int groupPosition) {
        return groupList[groupPosition];
    }
 
    public int getGroupCount() {
        return groupList.length;
    }
 
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
    	String groupName = (String) getGroup(groupPosition);
        
        if (convertView == null) {
            convertView = View.inflate(activity.getBaseContext(), R.layout.setting_group, null);
        }
        
        TextView title = (TextView) convertView.findViewById(R.id.title);
        title.setTypeface(null, Typeface.BOLD);
        title.setText(groupName);
        
        return convertView;
    }
 
    public boolean hasStableIds() {
        return true;
    }
 
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}