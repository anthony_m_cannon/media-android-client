package com.mediaplus.pageHandlers;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mediaplus.R;
import com.mediaplus.main.Constants;
import com.mediaplus.media.Film;
import com.mediaplus.media.Picture;
import com.mediaplus.media.Song;

public class MediaListAdapter extends ArrayAdapter<String> {
	private Context context;
	private ArrayList<?> mediaItems;

	public MediaListAdapter(Context context, int resource) {
		super(context, resource);
	}

	public MediaListAdapter(Context context, ArrayList<?> mediaItems,
			String[] listAmount) {
		super(context, R.layout.media_child, listAmount);

		this.context = context;
		this.mediaItems = mediaItems;
	}

	@Override
	public View getView(int id, View view, ViewGroup parent) {
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.media_child, parent, false);
		}

		ImageView cover = (ImageView) view.findViewById(R.id.cover);
		TextView firstLine = (TextView) view.findViewById(R.id.firstLine);
		TextView secondLine = (TextView) view.findViewById(R.id.secondLine);
		
		switch (Constants.mediaType) {
		case IMAGE:
			Picture image = (Picture) mediaItems.get(id);

			cover.setImageBitmap(image.getPoster());
			firstLine.setText(image.getTitle());
			break;
		case MUSIC:
			Song song = (Song) mediaItems.get(id);

			cover.setImageBitmap(song.getPoster());

			System.out.println("Setting Title: " + song.getTitle()
					+ " - Setting Genre: " + song.getGenre());
			firstLine.setText(song.getTitle());
			secondLine.setText(song.getGenre());
			break;
		case FILM:
			Film film = (Film) mediaItems.get(id);

			cover.setImageBitmap(film.getPoster());
			firstLine.setText(film.getTitle());
			secondLine.setText(film.getGenre());
			break;
		default:
			break;
		}

		return view;
	}
}
