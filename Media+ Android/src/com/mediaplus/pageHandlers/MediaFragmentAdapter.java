package com.mediaplus.pageHandlers;

import java.util.ArrayList;
import java.util.Locale;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mediaplus.main.Constants;
import com.mediaplus.media.handlers.MediaHandler;
import com.mediaplus.server.data.types.MediaType;

public class MediaFragmentAdapter extends FragmentPagerAdapter {
	private ArrayList<String> categories;
	private ArrayList<Integer> years;
	private String category;
	private MediaHandler media;
	
	public MediaFragmentAdapter(FragmentManager fm, MediaHandler media, String category) {
		super(fm);
		
		this.media = media;
		this.categories = media.film().getGenres();
		this.years = media.film().getYears();
		this.category = category.toLowerCase(Locale.getDefault());
	}

	@Override
	public Fragment getItem(int id) {
		Fragment f = null;

		if (id < getCount()) {
			switch(category) {
			case "genre":
				if (categories.size() > id) {
					f = new MediaFragment(categories.get(id), media);
				}
				break;
			case "year":
				if (years.size() > id) {
					f = new MediaFragment(years.get(id), media);
				}
				break;
			default:
				f = new MediaFragment(null, media);
				break;
			}
		}
		
		return f;
	}

	@Override
	public int getCount() {
		if (Constants.mediaType == MediaType.IMAGE || Constants.mediaType == MediaType.MUSIC) {
			return 1;
		} else {
			switch(category) {
			case "genre":
				return categories.size();
			case "year":
				return years.size();
			default:
				return 1;
			}
		}
	}
	
}