package com.mediaplus.main;

import android.app.Activity;

import com.mediaplus.media.handlers.MediaHandler;
import com.mediaplus.server.data.types.MediaType;
import com.mediaplus.server.handlers.ServerHandler;

public class Constants {
	public static Preferences pref;
	public static ServerHandler server;
	public static Activity activity;
	public static MediaHandler media;
	public static MediaType mediaType;
	public static boolean gotContent;
	
	public Constants() {
		pref = null;
		server = null;
		activity = null;
		media = null;
		mediaType = null;
		
		gotContent = false;
	}
}
