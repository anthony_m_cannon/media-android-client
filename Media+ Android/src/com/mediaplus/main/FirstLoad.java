package com.mediaplus.main;

public class FirstLoad {
	private static Preferences pref;
	
	public FirstLoad() {
		pref = Constants.pref;
		
		createStartingVariables();
		
		// start server wizard
		
		pref.saveBoolean("device-startUp", false);
	}


	private void createStartingVariables() { // Could get dialog boxes to ask user for preferences
		pref.saveString("layout-theme", "Android");
		pref.saveString("layout-filter", "All");
		
		pref.saveString("device-name", "Anthonys Phone");
		
		pref.saveString("server-name", "233 Server");
		
		// Testing
		pref.saveString("server-ip", "192.168.1.65");
		pref.saveInt("server-port", 9090);
	}
	
}
