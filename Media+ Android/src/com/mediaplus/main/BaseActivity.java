package com.mediaplus.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.mediaplus.R;
import com.mediaplus.controllers.ConnectionActivity;
import com.mediaplus.controllers.SettingsActivity;

public abstract class BaseActivity extends ActionBarActivity {
	private static Toast toast;
	
	public BaseActivity() {
		super();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Constants.activity = this;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		Constants.activity = this;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		
		Activity act = Constants.activity;
		
		if (id == R.id.action_settings) {
			if (!(act instanceof SettingsActivity) && !(act instanceof ConnectionActivity)) {
				launchActivity(SettingsActivity.class);
	
				return true;
			}
		}

		return super.onOptionsItemSelected(item);
	}

	public static void toast(final String message) {
		Constants.activity.runOnUiThread(new Runnable() {
			public void run() {
				if (toast != null) {
					toast.cancel();
				}
				
				toast = Toast.makeText(Constants.activity.getApplicationContext(),
						message, Toast.LENGTH_LONG);
				toast.show();
			}
		});
	}

	public static void launchActivity(Class<?> c) {
		launchActivity(c, null);
	}

	public static void launchActivity(Class<?> c, String data) {
		launchActivity(c, data, -1);
	}
	
	public static void launchActivity(Class<?> c, String data, int flags) {
		launch(c, data, flags);
	}
	
	// private
	private static void launch(Class<?> c, String data, int flags) {
		if (c != null) {
			Activity activity = Constants.activity;
			
			Intent intent = new Intent(activity, c);
			
			if (data != null) {
				if (!data.equals("")) {
					intent.putExtra("data", data);
				}
			}
			
			if (flags != -1) {
				intent.addFlags(flags);
			}
			
			activity.startActivity(intent);
		}
	}
}