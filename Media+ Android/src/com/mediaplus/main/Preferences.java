package com.mediaplus.main;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Preferences {
	private SharedPreferences sharedpreferences;
	private Editor editor;
	
	public Preferences(Activity activity) {
		sharedpreferences = activity.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
	}

	public boolean saveString(String key, String value) {
		editor = sharedpreferences.edit();
		editor.putString(key, value);
		
		return editor.commit();
	}
	
	public String getString(String key) {
		return sharedpreferences.getString(key, "");
	}
	
	public boolean saveInt(String key, int value) {
		editor = sharedpreferences.edit();
		editor.putInt(key, value);
		
		return editor.commit();
	}
	
	public int getInt(String key) {
		return sharedpreferences.getInt(key, 0);
	}
	
	public boolean saveBoolean(String key, boolean value) {
		editor = sharedpreferences.edit();
		editor.putBoolean(key, value);
		
		return editor.commit();
	}
	
	public boolean getBoolean(String key) {
		return sharedpreferences.getBoolean(key, true);
	}
}
