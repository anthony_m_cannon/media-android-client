package com.mediaplus.packets;

import com.mediaplus.server.data.types.PacketType;

public class CommandPacket extends Packet {
	public CommandPacket(int commandType, String data) {
		super(PacketType.COMMAND.id(), commandType, data);
	}
}
