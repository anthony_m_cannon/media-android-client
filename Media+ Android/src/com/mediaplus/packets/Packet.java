package com.mediaplus.packets;

import java.io.IOException;
import java.io.OutputStream;

public class Packet implements Runnable {
	private boolean packetCreated, sent;
	private String data;
	private OutputStream out;
	private int packetID, commandID;
	private String endPacket;
	
	public Packet(int packetID, int commandID, String data) {
		sent = false;
		packetCreated = false;
		endPacket = "0END0";
		
		this.packetID = packetID;
		this.commandID = commandID;
		this.data = data;
		
		packetCreated = true;
	}
	
	private byte[] buildPacket() {
		String packetString = "";
		
		if (packetID < 10) {
			packetString += "0";
		}
		
		packetString += packetID;
		
		if (commandID < 10) {
			packetString += "0";
		}
		
		packetString += commandID;
		packetString += data;
		packetString += String.valueOf(endPacket);
		
		return packetString.getBytes();
	}

	public boolean send(OutputStream out) {
		if (packetCreated) {
			this.out = out;
			
			Thread sending = new Thread(this);
			sending.start();
			
			while ( sending.isAlive() ) {};
		}
		
		packetCreated = false;
		return sent;
	}

	@Override
	public synchronized void run() {
		try {
			out.write(buildPacket());
			out.flush();
			
			sent = true;
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				System.err.println("ERROR: Couldn't sleep: " + e.getMessage());
			}
		} catch (IOException e) {
			sent = false;
		}
	}
}