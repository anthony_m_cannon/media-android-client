package com.mediaplus.packets;

import com.mediaplus.server.data.types.PacketType;

public class HandshakePacket extends Packet {
	private static final int ANDROID_HANDSHAKE = 02;
	
	public HandshakePacket() {
		super(PacketType.HANDSHAKE.id(), ANDROID_HANDSHAKE, "");
	}
}
