package com.mediaplus.server.data.types;

public enum PropertyType {
	ERROR(0x0), VOLUME(0x1), TOTAL_PLAY_TIME(0x2), VIDEO_CHANNEL(0x3), AUDIO_CHANNEL(0x4), SUBTITLE_CHANNEL(0x5), PAUSE_STATE(0x6), CURRENT_PLAY_TIME(0x7), NO_FILMS(0x8), FILM_POSTER(0x9);

	private int id;

	PropertyType(int id) {
		this.id = id;
	}

	public static PropertyType getPropertyType(int id) {
		switch (id) {
		default:
			return ERROR;
		case 0x0:
			return ERROR;
		case 0x1:
			return VOLUME;
		case 0x2:
			return TOTAL_PLAY_TIME;
		case 0x3: 
			return VIDEO_CHANNEL;
		case 0x4:
			return AUDIO_CHANNEL;
		case 0x5:
			return SUBTITLE_CHANNEL;
		case 0x6:
			return PAUSE_STATE;
		case 0x7:
			return CURRENT_PLAY_TIME;
		case 0x8:
			return NO_FILMS;
		case 0x9:
			return FILM_POSTER;
		
		}
	}

	public int id() {
		return id;
	}
}
