package com.mediaplus.server.data.types;

public enum MediaType {
	ERROR(0x0), FILM(0X1), MUSIC(0x2), IMAGE(0x3);
	
	private int id;

	MediaType(int id) {
		this.id = id;
	}

	public static MediaType getMediaType(int id) {
		switch (id) {
		default:
			return ERROR;
		case 0x0:
			return ERROR;
		case 0x1:
			return FILM;
		case 0x2:
			return MUSIC;
		case 0x3:
			return IMAGE;
		}
	}

	public int id() {
		return id;
	}
}
