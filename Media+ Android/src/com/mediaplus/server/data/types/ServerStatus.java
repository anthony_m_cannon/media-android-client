package com.mediaplus.server.data.types;

public enum ServerStatus {
	NOT_CONNECTED, CONNECTING, CONNECTED;
}
