package com.mediaplus.server.data.types;


public enum BooleanType {
	FALSE(0x0), TRUE(0X1);
	
	private int id;

	BooleanType(int id) {
		this.id = id;
	}

	public static BooleanType getBooleanType(int id) {
		switch (id) {
		default:
			return FALSE;
		case 0x0:
			return FALSE;
		case 0x1:
			return TRUE;
		}
	}

	public int id() {
		return id;
	}
}
