package com.mediaplus.server;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.mediaplus.R;
import com.mediaplus.controllers.InformationActivity;
import com.mediaplus.controllers.MainActivity;
import com.mediaplus.controllers.RemoteActivity;
import com.mediaplus.main.Constants;
import com.mediaplus.server.data.types.BooleanType;
import com.mediaplus.server.data.types.CommandType;
import com.mediaplus.server.data.types.PacketType;
import com.mediaplus.server.data.types.PropertyType;
import com.mediaplus.utils.DataParse;

public class ReceiverHandler {
	private String data;
	private DataParse dataParse;

	public ReceiverHandler(String data) {
		this.data = data;
		dataParse = new DataParse();

		run();
	}

	public void run() {
		try {
			if (data.length() < 2) {
				System.err.println("Data's length too small: " + data);
				return;
			}

			int packetType = Integer.parseInt(data.substring(0, 2));
			PacketType packageType = PacketType.getPacketType(packetType);
			data = data.substring(2);

			if (packageType == PacketType.COMMAND) {
				int commandType = Integer.parseInt(data.substring(0, 2));
				data = data.substring(2);

				CommandType ct = CommandType.getCommandType(commandType);

				switch (ct) {
				default:
					System.err.println("ERROR: Couldn't find Command Type: "
							+ ct + " " + commandType);
					break;
				case SET_PROPERTY:
					if (!(Constants.activity instanceof RemoteActivity))
						break;

					RemoteActivity ra = (RemoteActivity) Constants.activity;

					int propertyType = Integer.parseInt(data.substring(0, 2));
					data = data.substring(2);

					PropertyType propType = PropertyType
							.getPropertyType(propertyType);
//					System.out.println("PropertyType: " + propType.name());
					switch (propType) {
					default:

						break;
					case VOLUME:
						int volume = Integer.parseInt(data.substring(0, 3));

						ra.setVolumePosition(volume);
						break;
					case TOTAL_PLAY_TIME:
						long mil = Long.parseLong(data);
						if (mil != 0) {
							ra.setTotalPlayTime(mil);
						}
						break;
					case VIDEO_CHANNEL:

						break;
					case AUDIO_CHANNEL:

						break;
					case SUBTITLE_CHANNEL:

						break;
					case PAUSE_STATE:
						int pauseState = Integer.parseInt(data.substring(0, 1));

						BooleanType bt = BooleanType.getBooleanType(pauseState);
						boolean paused = Boolean.valueOf(bt.name());

						ra.setPlayingState(paused);
						break;
					case CURRENT_PLAY_TIME:
						mil = Integer.parseInt(data);
						if (mil != 0) {
							ra.setCurrentPlayTime(mil);
						}
						break;
					case FILM_POSTER:
						System.out.println("Poster: " + data);
						break;
					}
					break;
				case DISCONNECTED_DESKTOP:
					Constants.server.setRemoteAccess(false);
					break;
				case GET_PROPERTY:

					break;
				case BE_REMOTE:
					int generalType = Integer.parseInt(data);
					BooleanType gt = BooleanType.getBooleanType(generalType);

					if ((Constants.activity instanceof MainActivity)) {
						MainActivity activity = (MainActivity) Constants.activity;
						switch (gt) {
						default:
							activity.pinDenied();
							break;
						case FALSE:
							activity.pinDenied();
							break;
						case TRUE:
							activity.pinAccepted();
							break;
						}
					} else {
						Activity act = Constants.activity;
						if (act instanceof InformationActivity) {
							InformationActivity activity = (InformationActivity) act;
							switch (gt) {
							default:
								activity.pinDenied();
								break;
							case FALSE:
								activity.pinDenied();
								break;
							case TRUE:
								activity.pinAccepted();
								break;
							}
						}
					}
					break;
				case INFO:
					String[] values = data.split("�");
					HashMap<String, String> hashMap = new HashMap<String, String>();

					for (String value : values) {
						String[] map = value.split("=");
						hashMap.put(map[0], map[1]);
					}

					int runtime = Integer.parseInt(hashMap.get("runtime")
							.split(" ")[0]);
					String[] directors = hashMap.get("director").split(", ");
					String[] actors = hashMap.get("actors").split(", ");
					Bitmap poster = getBitmapFromURL(hashMap.get("poster"));

					Constants.media.film().addFilmInfo(poster,
							hashMap.get("title"),
							Integer.parseInt(hashMap.get("year")), runtime,
							hashMap.get("genre"),
							Double.parseDouble(hashMap.get("imdbRating")),
							Double.parseDouble(hashMap.get("tomatoRating")),
							directors, actors, hashMap.get("plot"),
							hashMap.get("rated"), hashMap.get("country"));
					break;
				case GET_FILMS:
					int point = data.lastIndexOf(".");
					values = new String[2];
					values[0] = data.substring(0, point);
					values[1] = data.substring(point + 1);

					Constants.media.film().newFilm(values[0], values[1]);

					Constants.server.control().info(data);

					break;
				case GET_IMAGES:
					try {
						String[] value = data.split("�");

						String name = value[0];
						String title = name.substring(0, name.lastIndexOf('.'));
						String extention = name
								.substring(name.lastIndexOf('.') + 1);

						String imageBase = value[1];
						byte[] image = Base64.decode(imageBase, Base64.DEFAULT);
						Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0,
								image.length);

						Constants.media.image().newImage(bitmap, title,
								extention);
					} catch (Exception e) {
						e.printStackTrace();
					}
					break;
				case GET_MUSIC:
					point = data.lastIndexOf(".");
					values = new String[2];
					values[0] = data.substring(0, point);
					values[1] = data.substring(point + 1);

					Constants.media.music().newSong(values[0], values[1]);
					break;
				}
			}
		} catch (NumberFormatException e) {
			dataParse.tryGetData(data);

			return;
		} catch (Exception e) {
			System.err.println((e.getMessage() == null) ? "" : e.getMessage());
		}
	}

	// private
	private Bitmap getBitmapFromURL(String link) {
		try {
			URL url = new URL(link);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);

			return myBitmap;

		} catch (IOException e) {
//			System.err.println("Cant get image: " + link);

			Bitmap image = BitmapFactory.decodeResource(
					Constants.activity.getResources(), R.drawable.pna);

			return image;
		}

	}
}
