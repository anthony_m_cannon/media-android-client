package com.mediaplus.server.handlers;

import com.mediaplus.server.data.types.CommandType;
import com.mediaplus.server.data.types.PropertyType;

public class ServerMediaHandler {
	private ServerHandler handler;
	
	public ServerMediaHandler(ServerHandler serverHandler) {
		handler = serverHandler;
	}
	
	public boolean playPause() {
		return handler.sendCommand(CommandType.PLAY_PAUSE.id());
	}

	public boolean stop() {
		return handler.sendCommand(CommandType.STOP.id());
	}

	public boolean setVolumeLevel(int pos) {
		return setProperty(PropertyType.VOLUME.id(), String.valueOf(pos));
	}

	public boolean setMediaPosition(final long pos) {
		return setProperty(PropertyType.CURRENT_PLAY_TIME.id(), String.valueOf(pos));
	}

	public boolean mute() {
		return handler.sendCommand(CommandType.MUTE.id());
	}
	
	// property
	public boolean getPoster(String title) {
		return getProperty(PropertyType.FILM_POSTER.id(), title);
	}

	public boolean getFilmAmount() {
		return getProperty(PropertyType.NO_FILMS.id());
	}
	
	// private
	private boolean setProperty(int property, String data) {
		return handler.sendCommand(CommandType.SET_PROPERTY.id(), makeDoubleDigitString(property) + data);
	}
	
	private boolean getProperty(int property) {
		return getProperty(CommandType.GET_PROPERTY.id(), makeDoubleDigitString(property));
	}
	
	private boolean getProperty(int property, String data) {
		return handler.sendCommand(CommandType.GET_PROPERTY.id(), makeDoubleDigitString(property) + data);
	}
	
	private String makeDoubleDigitString(int number) {
		String data = "";
		if (number < 10) {
			data += "0";
		}
		data += number;
		
		return data;
	}
}
