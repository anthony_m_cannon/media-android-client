package com.mediaplus.server.handlers;

import com.mediaplus.server.Server;

public class ServerHandler extends Server {
	private ServerMediaHandler media;
	private ServerControlHandler control;
	
	public ServerHandler() {
		media = new ServerMediaHandler(this);
		control = new ServerControlHandler(this);
	}
	
	public ServerMediaHandler media() {
		return media;
	}
	
	public ServerControlHandler control() {
		return control;
	}
}
