package com.mediaplus.server.handlers;

import com.mediaplus.server.data.types.CommandType;

public class ServerControlHandler {
	private ServerHandler handler;
	
	public ServerControlHandler(ServerHandler serverHandler) {
		handler = serverHandler;
	}
	
	public boolean info(String fileName) {
		return handler.sendCommand(CommandType.INFO.id(), fileName);
	}

	public boolean streamFilm(String film) {
		return handler.sendCommand(CommandType.STREAM_FILM.id(), film);
	}

	public boolean showImage(String image) {
		return handler.sendCommand(CommandType.SHOW_IMAGE.id(), image);
	}

	public boolean playSong(String song) {
		return handler.sendCommand(CommandType.PLAY_SONG.id(), song);
	}

	public boolean getFilms() {
		return handler.sendCommand(CommandType.GET_FILMS.id());
	}

	public boolean getImages() {
		return handler.sendCommand(CommandType.GET_IMAGES.id());
	}

	public boolean getMusic() {
		return handler.sendCommand(CommandType.GET_MUSIC.id());
	}

	public boolean beRemote(String pin) {
		return handler.sendCommand(CommandType.BE_REMOTE.id(), pin);
	}
}
