package com.mediaplus.server.connection;

import com.mediaplus.controllers.ConnectionActivity;
import com.mediaplus.main.Constants;

public class ConnectionServerConnectionTask extends ServerConnectionTask {
	private ConnectionActivity activity;
	
	public ConnectionServerConnectionTask(String ip, int port) {
		super(ip, port);
		
		activity = (ConnectionActivity) Constants.activity;
	}
	
	@Override
	protected void onProgressUpdate(String... values) {
		activity.setProgressTxt(values[0]);
	}
	
	@Override
	protected void finished() {
		super.finished();
		activity.serverFinish();
	}
	
	@Override
	protected void error() {
		super.error();
		activity.serverError();
	}
}
