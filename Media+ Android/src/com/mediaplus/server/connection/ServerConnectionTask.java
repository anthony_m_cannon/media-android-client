package com.mediaplus.server.connection;

import android.os.AsyncTask;

import com.mediaplus.controllers.MainActivity;
import com.mediaplus.main.BaseActivity;
import com.mediaplus.main.Constants;
import com.mediaplus.server.data.types.ServerStatus;
import com.mediaplus.server.handlers.ServerHandler;

/**
 * Represents an asynchronous login/registration task used to authenticate the
 * user.
 */
public class ServerConnectionTask extends AsyncTask<Void, String, Boolean> {
	private final String mIp;
	private final int mPort;
	private ServerHandler server;

	public ServerConnectionTask(String ip, int port) {
		mIp = ip;
		mPort = port;
		
		server = Constants.server;
	}

	@Override
	protected Boolean doInBackground(Void... params) {
		if (server.isConnected()) {
			server.stopServer();
			System.out.println("Stopping server...");
			
			Constants.server = new ServerHandler();
			server = Constants.server;
		}
		
		int connectionTries = 1;
		while (server.status() == ServerStatus.NOT_CONNECTED && connectionTries < 4) {
			if (connectionTries > 1) {
				publishProgress("Atempting to connect to server..."
						+ "\n" + "Atempt: " + String.valueOf(connectionTries));
			}

			server.connect(mIp, mPort);

			while (server.status() == ServerStatus.CONNECTING);

			if (server.status() == ServerStatus.CONNECTED) {
//				if (!Constants.gotContent) {
					publishProgress("Collecting Media.");
					getMedia();
//				}
				
				while (Constants.server.getSr().getTimer() < 5);
				
				return true;
			}

			connectionTries++;

		}

		return false;
	}

	private void getMedia() {
		Constants.server.control().getFilms();
		Constants.server.control().getImages();
		Constants.server.control().getMusic();
		Constants.gotContent = true;
	}
	
	@Override
	protected void onProgressUpdate(String... values) {
	}

	@Override
	protected void onPostExecute(final Boolean success) {
		if (success) {
			finished();
		} else {
			error();
		}
	}

	@Override
	protected void onCancelled() {
		error();
	}
	
	
	protected void finished() {
		BaseActivity.toast("Connected succesfully.");

		BaseActivity.launchActivity(MainActivity.class);
	}
	
	protected void error() {
		BaseActivity.toast("Failed to connect.");
	}
}