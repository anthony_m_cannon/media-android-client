package com.mediaplus.server.connection;

import com.mediaplus.controllers.SplashActivity;
import com.mediaplus.main.Constants;

public class SplashServerConnectionTask extends ServerConnectionTask {
	private SplashActivity activity;
	
	public SplashServerConnectionTask(String ip, int port) {
		super(ip, port);
		
		activity = (SplashActivity) Constants.activity;
	}
	
	@Override
	protected void onProgressUpdate(String... values) {
		activity.setProgressTxt(values[0]);
	}
	
	@Override
	protected void finished() {
		super.finished();
		activity.serverFinish();
	}
	
	@Override
	protected void error() {
		super.error();
		activity.serverError();
	}
}
