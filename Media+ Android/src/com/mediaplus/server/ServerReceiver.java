package com.mediaplus.server;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import com.mediaplus.main.Constants;
import com.mediaplus.utils.Timer;

public class ServerReceiver implements Runnable {
	private InputStream is;
	private final String endPacket;
	private Thread timerThread;
	private Timer timer;
	
	public ServerReceiver(InputStream inputStream) {
		this.is = inputStream;
		endPacket = "0END0";
		newTimer();
	}
	
	public void start() {
		Thread t = new Thread(this);
		t.start();
	}

	@Override
	public synchronized void run() { 
		System.out.println("running");
		while(Constants.server.isConnected()) {
			try {
				byte[] packetBytes = new byte[Character.MAX_VALUE];
	            while(is.read(packetBytes) != -1) {
	            	String packetString = new String(packetBytes, Charset.forName("UTF-8")).trim();
	            	
	            	String[] packets = packetString.split(String.valueOf(endPacket));
	            	
	            	for (String packet : packets) {
	            		new ReceiverHandler(packet.trim());
	            	}
	            	
	            	newTimer();
	            }
			} catch (Exception e) {
				System.err.println(e.getMessage());
				Constants.server.stopServer();
			}
		}
	}
	
	public int getTimer() {
		return timer.getTime();
	}
	
	private void newTimer() {
		if (timerThread != null) if (timerThread.isAlive()) timerThread.interrupt();
		
		timer = new Timer();
		timerThread = new Thread(timer);
		
		timerThread.start();
	}

	public void close() throws IOException { // only call from outside class
		if (is != null) is.close();
	}
	
}
