package com.mediaplus.server;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.mediaplus.controllers.ConnectionActivity;
import com.mediaplus.controllers.MainActivity;
import com.mediaplus.controllers.RemoteActivity;
import com.mediaplus.controllers.SettingsActivity;
import com.mediaplus.controllers.SplashActivity;
import com.mediaplus.main.BaseActivity;
import com.mediaplus.main.Constants;
import com.mediaplus.packets.CommandPacket;
import com.mediaplus.packets.HandshakePacket;
import com.mediaplus.server.data.types.ServerStatus;

public class Server implements Runnable {
	private Socket socket;
	private OutputStream out;
	private ServerReceiver sr;
	private String ip;
	private int port;
	private ServerStatus status;
	private boolean connected;
	private boolean remoteAccess;

	public Server() {
		startingVariables();
	}
	
	public void connect(String ip, int port) {
		status = ServerStatus.CONNECTING;
		connected = false;
		this.ip = ip;
		this.port = port;

		ConnectivityManager connectivityManager = (ConnectivityManager) Constants.activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

		if (networkInfo != null && networkInfo.isConnected()
				&& networkInfo.isAvailable()) {
			Thread connecting = new Thread(this);
			connecting.start();
		} else {
			BaseActivity.toast("No wifi connecion.");
		}
	}

	public boolean stopServer() {
		try {
			Activity activity = Constants.activity;
			
			if (isConnected()) {
				if (!(activity instanceof SettingsActivity)
						&& !(activity instanceof SplashActivity)
						&& !(activity instanceof MainActivity)
						&& !(activity instanceof ConnectionActivity)) {
					BaseActivity.launchActivity(MainActivity.class);
				}
			}

			if (sr != null)
				sr.close();
			if (out != null)
				out.close();
			if (socket != null)
				socket.close();

			startingVariables();
			
			if (!(activity instanceof ConnectionActivity)
					&& !(activity instanceof SplashActivity)) {
				BaseActivity.toast("Server Disconnected.");
			}
			
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

	}

	public boolean sendCommand(int commandType, String data) {
		CommandPacket commandPacket = new CommandPacket(commandType, data);

		return commandPacket.send(out);
	}

	public boolean sendCommand(int commandType) {
		return sendCommand(commandType, "");
	}

	public boolean isConnected() {
		return connected;
	}

	public ServerStatus status() {
		return status;
	}

	public ServerReceiver getSr() {
		return sr;
	}

	public OutputStream outputStream() {
		return out;
	}

	public boolean remoteAccess() {
		return remoteAccess;
	}

	public void setRemoteAccess(boolean access) {
		remoteAccess = access;
		
		if (noAccess()) {
			Activity act = Constants.activity;
			if (act instanceof RemoteActivity) {
				BaseActivity.launchActivity(MainActivity.class);
				BaseActivity.toast("Desktop Disconnected.");
			}
		}
	}

	// Core
	@Override
	public void run() {
		if (!connected) {
			try {
				tryConnectToServer();

				out = socket.getOutputStream();

				if (sendHandshake()) {
					sr = new ServerReceiver(socket.getInputStream());
					sr.start();

					connected = true;
					status = ServerStatus.CONNECTED;
				}
			} catch (IOException e) {
				e.printStackTrace();
				stopServer();
			}
		}
	}

	// Private
	private void tryConnectToServer() throws IOException {
		socket = new Socket();

		int timeOut = 1000;
		InetSocketAddress isa = new InetSocketAddress(ip, port);

		socket.connect(isa, timeOut);
	}

	private boolean sendHandshake() {
		HandshakePacket handshakePacket = new HandshakePacket();

		return handshakePacket.send(out);
	}

	private void startingVariables() {
		connected = false;
		setRemoteAccess(false);
		status = ServerStatus.NOT_CONNECTED;
	}

	private boolean noAccess() {
		if (remoteAccess) return false;
		return true;
	}
}
