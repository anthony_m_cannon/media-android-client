package com.mediaplus.filter;


public class ImageFilter extends Filter {
	private static final String[] EXTENSIONS_IMAGE = {"jpg", "png"};
	
	public boolean accept(String name) {
        return super.accept(name, EXTENSIONS_IMAGE);
    }
}
