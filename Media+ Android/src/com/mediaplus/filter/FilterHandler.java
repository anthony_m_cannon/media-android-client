package com.mediaplus.filter;

import com.mediaplus.server.data.types.MediaType;

public class FilterHandler {
	private AudioFilter audioF;
	private VideoFilter videoF;
	private ImageFilter imageF;
	
	public FilterHandler() {
		audioF = new AudioFilter();
		videoF = new VideoFilter();
		imageF = new ImageFilter();
	}
	
	public MediaType accept(String name) {
		if (audioF.accept(name)) {
			return MediaType.MUSIC;
		} else if (videoF.accept(name)) {
			return MediaType.FILM;
		} else if (imageF.accept(name)) {
			return MediaType.IMAGE;
		} else {
			return MediaType.ERROR;
		}
	}
}
