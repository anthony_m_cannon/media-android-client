package com.mediaplus.filter;

import java.util.Locale;

public class Filter {
	protected boolean accept(String name, String[] EXTENSIONS) {
        if(name != null && EXTENSIONS != null) {
        	if (!name.equals("") && EXTENSIONS.length != 0) {
        		int dot = name.lastIndexOf('.');
                String fileExtension = name.substring(dot + 1);
                fileExtension = fileExtension.toLowerCase(Locale.getDefault());
                
	            for (String extension : EXTENSIONS) {
	                if (fileExtension.equals(extension)) {
	                	return true;
	                }
	            }
        	}
        }
        
        return false;
    }
}
