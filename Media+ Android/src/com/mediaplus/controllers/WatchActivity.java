package com.mediaplus.controllers;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import com.mediaplus.R;
import com.mediaplus.main.BaseActivity;
import com.mediaplus.main.Constants;
import com.mediaplus.media.handlers.MediaHandler;
import com.mediaplus.models.WatchModel;
import com.mediaplus.pageHandlers.MediaFragmentAdapter;

public class WatchActivity extends BaseActivity {
	private final WatchModel model;
	private ViewPager viewPager;

	public WatchActivity() {
		super();
		
		model = new WatchModel();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_watch);
		
		FragmentManager fm = getSupportFragmentManager();
		viewPager = (ViewPager) findViewById(R.id.viewPager);
		MediaHandler mh = model.media();
		String filter = Constants.pref.getString("layout-filter");
		MediaFragmentAdapter mfa = new MediaFragmentAdapter(fm, mh, filter);
		viewPager.setAdapter(mfa);
		
	}
	
	
}