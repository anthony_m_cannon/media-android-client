package com.mediaplus.controllers;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mediaplus.R;
import com.mediaplus.main.BaseActivity;
import com.mediaplus.models.ConnectionModel;
import com.mediaplus.server.connection.ConnectionServerConnectionTask;
import com.mediaplus.utils.ServerValidator;

/**
 * A login screen that offers login via ip/port.
 */
public class ConnectionActivity extends BaseActivity {
	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private ConnectionServerConnectionTask mAuthTask = null;
	private ServerValidator mValidator;

	// UI references.
	private EditText mIpView, mPortView;
	private TextView mProgressTxt;
	private View mProgressBar;
	private View mConnectionInfoFormView, mConnectionProgressFormView;
	private ConnectionModel model;

	public ConnectionActivity() {
		super();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_connection);

		mValidator = new ServerValidator();
		model = new ConnectionModel();

		// Set up the connection form.
		mIpView = (EditText) findViewById(R.id.ip);
		mPortView = (EditText) findViewById(R.id.port);
		mPortView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.connect || id == EditorInfo.IME_NULL) {
							attemptConnection();
							return true;
						}
						return false;
					}
				});

		Button mServerConnectionButton = (Button) findViewById(R.id.server_connect_button);
		mServerConnectionButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (networkAvailable()) {
							
					attemptConnection();
				}
			}
		});

		mConnectionInfoFormView = findViewById(R.id.connection_info_form);
		mConnectionProgressFormView = findViewById(R.id.connection_progress_form);
		mProgressBar = findViewById(R.id.connection_progress_bar);
		mProgressTxt = (TextView) findViewById(R.id.connection_progress_text);

		mIpView.setText(model.getIp());
		mPortView.setText(String.valueOf(model.getPort()));
	}

	public void serverFinish() {
		setMAuthTask(null);
		showProgress(false);
	}

	public void serverError() {
		setMAuthTask(null);
		showProgress(false);
		
		getMPortView().setError(getString(R.string.error_incorrect_port));
		getMPortView().requestFocus();
	}

	private boolean networkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();

		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	public void setProgressTxt(String text) {
		mProgressTxt.setText(text);
	}

	public void setMAuthTask(ConnectionServerConnectionTask sct) {
		mAuthTask = sct;
	}

	public EditText getMPortView() {
		return mPortView;
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptConnection() {
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mIpView.setError(null);
		mPortView.setError(null);

		// Reset text
		mProgressTxt.setText("Connect to server...");

		// Store values at the time of the login attempt.
		String ip = mIpView.getText().toString();
		String port = mPortView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password, if the user entered one.
		if (TextUtils.isEmpty(port)) {
			mPortView.setError(getString(R.string.error_field_required));
			focusView = mPortView;
			cancel = true;
		} else if (!isPortValid(port)) {
			mPortView.setError(getString(R.string.error_invalid_port));
			focusView = mPortView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(ip)) {
			mIpView.setError(getString(R.string.error_field_required));
			focusView = mIpView;
			cancel = true;
		} else if (!isIpValid(ip)) {
			mIpView.setError(getString(R.string.error_invalid_ip));
			focusView = mIpView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			int mPort = Integer.parseInt(port);
			showProgress(true);

			hideKeyBoard();
			mAuthTask = new ConnectionServerConnectionTask(ip, mPort);
			mAuthTask.execute((Void) null);

			model.setIp(ip);
			model.setPort(mPort);
		}
	}

	private boolean isIpValid(String ip) {
		return mValidator.ipValidate(ip);
	}

	private boolean isPortValid(String port) {
		int pass = Integer.parseInt(port);
		return mValidator.portValidate(pass);
	}
	
	private void hideKeyBoard() {
		InputMethodManager imm = (InputMethodManager)getSystemService(
				  Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getMPortView().getWindowToken(), 0);
				imm.hideSoftInputFromWindow(mIpView.getWindowToken(), 0);
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mConnectionInfoFormView.setVisibility(show ? View.GONE
					: View.VISIBLE);
			mConnectionInfoFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mConnectionInfoFormView
									.setVisibility(show ? View.GONE
											: View.VISIBLE);
						}
					});

			mConnectionProgressFormView.setVisibility(show ? View.VISIBLE
					: View.GONE);
			mProgressBar.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mProgressBar.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mConnectionProgressFormView.setVisibility(show ? View.VISIBLE
					: View.GONE);
			mConnectionInfoFormView.setVisibility(show ? View.GONE
					: View.VISIBLE);
		}
	}
}
