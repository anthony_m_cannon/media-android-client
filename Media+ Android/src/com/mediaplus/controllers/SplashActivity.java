package com.mediaplus.controllers;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.mediaplus.R;
import com.mediaplus.main.BaseActivity;
import com.mediaplus.main.Constants;
import com.mediaplus.main.FirstLoad;
import com.mediaplus.main.Preferences;
import com.mediaplus.media.handlers.MediaHandler;
import com.mediaplus.server.connection.SplashServerConnectionTask;
import com.mediaplus.server.handlers.ServerHandler;

public class SplashActivity extends Activity {
	private SplashServerConnectionTask mAuthTask;
	private View mProgressBar;
	private TextView connectionProgressText;

	public SplashActivity() {
		super();
		
		mAuthTask = null;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		mProgressBar = findViewById(R.id.progress_bar);
		connectionProgressText = (TextView) findViewById(R.id.connection_progress_text);
		
		initializeConstants();
		connectToServer();
	}
	
	@Override
	protected void onResume() {
		if (Constants.activity instanceof MainActivity) {
			finish();
			BaseActivity.launchActivity(SplashActivity.class);
		}
		
		super.onResume(); // change Constant.activity to this
	}
	
	public void serverFinish() {
		mAuthTask = null;
	}

	public void serverError() {
		mAuthTask = null;
		BaseActivity.launchActivity(MainActivity.class, "splash");
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mProgressBar.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0);
		}
	}
	
	public void setProgressTxt(String text) {
		connectionProgressText.setText(text);
	}

	// private
	private void initializeConstants() {
		Constants.activity = this;
		Constants.pref = new Preferences(this);
		
		if (firstTimeLoaded()) {
			new FirstLoad();
			// send to connectionActivity
		}

		Constants.media = new MediaHandler();
		Constants.server = new ServerHandler();
	}
	
	private void connectToServer() {
		mAuthTask = new SplashServerConnectionTask(Constants.pref.getString("server-ip"), Constants.pref.getInt("server-port"));
		mAuthTask.execute((Void) null);
	}
	
	private boolean firstTimeLoaded() {
		return Constants.pref.getBoolean("device-startUp");
	}
}
