package com.mediaplus.controllers;

import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.mediaplus.R;
import com.mediaplus.main.BaseActivity;
import com.mediaplus.main.Constants;
import com.mediaplus.media.Film;
import com.mediaplus.media.Picture;
import com.mediaplus.media.Song;
import com.mediaplus.media.handlers.MediaHandler;
import com.mediaplus.remote.listeners.RemoteButtonListener;
import com.mediaplus.remote.listeners.RemoteSeekListener;
import com.mediaplus.server.data.types.MediaType;
import com.mediaplus.utils.MediaConstants;

public class RemoteActivity extends BaseActivity {
	private ImageButton[] buttons;
	private SeekBar volumeBar, positionBar;
	private TextView volumeBarTxt, positionBarTotalTxt, positionBarCurrentTxt;
	private String itemName;
	private ImageView image;

	public RemoteActivity() {
		super();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_remote);

		getElements();
		setElementInformation();

		setActionListeners();

		Bundle extras = getIntent().getExtras();
		
		if (extras != null) {
			itemName = extras.getString("data");
			MediaConstants.playing = true;
			
			MediaHandler mh = Constants.media;
			MediaType mt = mh.getMediaTypeByTitle(itemName);
			switch (mt) {
			default: // error
			case ERROR:
				System.err.println("Couldn't find media type: " + mt);
				break;
			case FILM:
				Film film = mh.film().getByExactName(itemName);
				if (film != null) {
					image.setImageBitmap( film.getPoster() );
				}
				break;
			case IMAGE:
				Picture picture = mh.image().getByExactName(itemName);
				if (picture != null) {
					image.setImageBitmap( picture.getPoster() );
				}
				break;
			case MUSIC:
				Song song = mh.music().getByExactName(itemName);
				if (song != null) {
					image.setImageBitmap( song.getPoster() );
				}
				break;
			
			}
		} else {
			image.setImageResource(R.drawable.pna);
		}

		setPlayingState(MediaConstants.playing);
	}

	// Gets
	public boolean getPlayingState() {
		return MediaConstants.playing;
	}

	public String getItemName() {
		return itemName;
	}

	// Sets
	public void setVolume(int volume) {
		setVolumePosition(volume);
		Constants.server.media().setVolumeLevel(volume);
	}

	public void setPosition(long position) {
		setCurrentPlayTime(position);
		Constants.server.media().setMediaPosition(position);
	}

	public void setVolumePosition(final int volume) {
		int vol = volume;
		
		if (vol < 0) {
			vol = 0;
			setMuteState(true);
		}
		
		if (vol > 100) {
			vol = 100;
			setMuteState(false);
		}

		setVolumeBarAndText(volume,
				String.valueOf(volume));
	}

	public void setCurrentPlayTime(final long mil) {
		int sec = (int) mil / 1000;
		int max = positionBar.getMax();

		if (sec < 0)
			sec = 0;
		if (sec > max)
			sec = max;
		int min = sec / 60;

		setPositionBarAndText(sec,
				getTime(sec, min));
	}
	
	public void setTotalPlayTime(final long mil) {
		String text = positionBarTotalTxt.getText().toString();
		if (text.equals("00:00")) {
			this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					int sec = (int) (mil / 1000);
					int min = sec / 60;

					positionBarTotalTxt.setText(getTime(sec, min));
					positionBar.setMax(sec);
				}
			});
		}
	}

	public void setMuteState(final boolean muted) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (muted) {
					buttons[3] // silent
							.setImageResource(R.drawable.btn_muteon);
				} else {
					buttons[3] // loud
							.setImageResource(R.drawable.btn_muteoff);
				}
				MediaConstants.muted = muted;
			}
		});
	}

	public void setPlayingState(final boolean playing) {
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (playing) {
					buttons[0]
							.setImageResource(R.drawable.btn_pause);
				} else { // play
					buttons[0]
							.setImageResource(R.drawable.btn_play);
				}
				MediaConstants.playing = playing;
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		default:
			super.onKeyDown(keyCode, event);
			break;
		case KeyEvent.KEYCODE_VOLUME_UP:
			int volume = getVolumelevel() + 10;
			setVolume(volume);
			break;
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			volume = getVolumelevel() - 10;
			setVolume(volume);
			break;
		}

		return true;
	}

	public void setMute() {
		if (isMuted()) {
			setMuteState(false);
		} else {
			setMuteState(true);
		}
		
		Constants.server.media().mute();
	}

	// private

	private void setElementInformation() {
		int length = 100;

		volumeBar.setMax(100);
		positionBar.setMax(length);
	}

	private void getElements() {
		getButtons();
		getBars();
		getTextViews();
		getImageViews();
	}

	private void getImageViews() {
		image = (ImageView) findViewById(R.id.imageView);
	}

	private void getTextViews() {
		volumeBarTxt = (TextView) findViewById(R.id.volumeBarTxt);
		positionBarTotalTxt = (TextView) findViewById(R.id.positionBarTotalTxt);
		positionBarCurrentTxt = (TextView) findViewById(R.id.positionBarCurrentTxt);
	}

	private void getButtons() {
		buttons = new ImageButton[4];

		buttons[0] = (ImageButton) findViewById(R.id.playPauseBtn);
		buttons[1] = (ImageButton) findViewById(R.id.infoBtn);
		buttons[2] = (ImageButton) findViewById(R.id.stopBtn);
		buttons[3] = (ImageButton) findViewById(R.id.muteBtn);
	}

	private void getBars() {
		volumeBar = (SeekBar) findViewById(R.id.volumeBar);
		positionBar = (SeekBar) findViewById(R.id.positionBar);
	}

	private void setActionListeners() {
		setButtonListeners();
		setSeekListeners();
	}

	private void setButtonListeners() {
		RemoteButtonListener rbl = new RemoteButtonListener();

		for (ImageButton b : buttons) {
			b.setOnClickListener(rbl);
		}
	}

	private void setSeekListeners() {
		RemoteSeekListener rsl = new RemoteSeekListener();

		volumeBar.setOnSeekBarChangeListener(rsl);
		positionBar.setOnSeekBarChangeListener(rsl);
	}

	private String stringLengthMin(String string) {
		while (string.length() < 2) {
			string = "0" + string;
		}

		return string;
	}

	private String getTime(int sec, int min) {
		String time = stringLengthMin(String.valueOf(min)) + ":"
				+ stringLengthMin(String.valueOf(sec % 60));

		return time;
	}

	public int getVolumelevel() {
		return Integer.parseInt(volumeBarTxt.getText().toString());
	}
	
	private boolean isMuted() {
		return MediaConstants.muted;
	}
	
	private void setPositionBarAndText(final int progress,
			final String progressTxt) {
			this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
//					if (positionBar.getProgress() != progress) {
						// reset
						positionBar.setProgress(progress);
						positionBar.setMax(positionBar.getMax());

						positionBar.setProgress(progress);
						positionBarCurrentTxt.setText(progressTxt);
//					}
				}
			});
		}
		
		private void setVolumeBarAndText(final int progress,
			final String progressTxt) {
			this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
//					if (volumeBar.getProgress() != progress) {
						// reset
						volumeBar.setProgress(progress);
						volumeBar.setMax(100);

						volumeBar.setProgress(progress);
						volumeBarTxt.setText(progressTxt);
//					}
				}
			});
		}

}
