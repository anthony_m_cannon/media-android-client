package com.mediaplus.controllers;

import java.util.Locale;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;

import com.mediaplus.R;
import com.mediaplus.dialogs.SettingsDialogs;
import com.mediaplus.main.BaseActivity;
import com.mediaplus.main.Constants;
import com.mediaplus.models.SettingsModel;
import com.mediaplus.pageHandlers.SettingsListAdapter;

public class SettingsActivity extends BaseActivity {
	private SettingsModel model;
	private SettingsListAdapter expListAdapt;
	private ExpandableListView expListView;
	private SettingsDialogs dialog;

	public SettingsActivity() {
		super();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		model = new SettingsModel();

		expListAdapt = new SettingsListAdapter(this, model.groupList(),
				model.listCollection());

		expListView = (ExpandableListView) findViewById(R.id.settings_list);
		expListView.setAdapter(expListAdapt);

		expListView.setOnChildClickListener(onChildClick);

		for (int i = 0; i < expListAdapt.getGroupCount(); i++) {
			expListView.expandGroup(i);
		}

		dialog = new SettingsDialogs(this);
	}

	public void update() {
		launchActivity(this.getClass());
	}

	public String getSelected(int groupPosition, int childPosition) {
		String[] selected = (String[]) expListAdapt.getChild(groupPosition,
				childPosition);
		
		 // 1 is the right text
		return selected[0].substring(0, selected[0].length() - 1);
	}

	OnChildClickListener onChildClick = new OnChildClickListener() {
		@Override
		public boolean onChildClick(ExpandableListView parent, View v,
				int groupPosition, int childPosition, long id) {

			String selected = getSelected(groupPosition, childPosition);
			selected = selected.toLowerCase(Locale.getDefault());

			switch (selected) {
			case "filter":
				dialog.filter(FilterOnClick);
				break;
			case "name": // Server Name
				dialog.getServerNameDialog(model.getServerName(), ServerNameOnClick);
				break;
			case "ip":
				launchActivity(ConnectionActivity.class);
				break;
			case "port":
				launchActivity(ConnectionActivity.class);
				break;
			case "device name":
				dialog.getDeviceNameDialog(model.getDeviceName(), DeviceNameOnClick);
				break;
			}

			return true;
		}
	};

	DialogInterface.OnClickListener ServerNameOnClick = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface di, int id) {
			switch (id) {
			case -1: // Ok
				String name = (String) dialog.getEntry().getText().toString();
				if (!name.equals("") && !name.equals(model.getServerName())) {
					model.setServerName(name);
					update();
				}
				break;
			case -2: // Cancel
				// Do Nothing
				break;
			}
		}
	};

	DialogInterface.OnClickListener DeviceNameOnClick = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface di, int id) {
			switch (id) {
			case -1: // Ok

				String name = (String) dialog.getEntry().getText().toString();
				if (!name.equals("") && !name.equals(model.getDeviceName())) {
					model.setDeviceName(name);
					update();
				}
				break;
			case -2: // Cancel
				// Do Nothing
				break;
			}
		}
	};
	
	DialogInterface.OnClickListener FilterOnClick = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface di, int id) {
			switch (id) {
			case -1: // Ok
				Constants.pref.saveString("layout-filter", dialog.getSelectedName());
				update();
				System.out.println(dialog.getSelectedName());
				break;
			case -2: // Cancel
				// Do Nothing
				break;
			default:
				dialog.setSelected(id);
				break;
			}
		}
	};

}