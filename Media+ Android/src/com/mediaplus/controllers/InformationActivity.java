package com.mediaplus.controllers;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.mediaplus.R;
import com.mediaplus.dialogs.InformationDialogs;
import com.mediaplus.main.BaseActivity;
import com.mediaplus.main.Constants;
import com.mediaplus.media.Film;
import com.mediaplus.media.Picture;
import com.mediaplus.media.Song;
import com.mediaplus.media.handlers.MediaHandler;
import com.mediaplus.models.InformationModel;

public class InformationActivity extends BaseActivity {
	private InformationModel model;
	private InformationDialogs dialogs;
	
	private String itemName;
	
	// film
	private ImageButton streamBtn;
	private ImageView cover, certificate;
	private TextView filmTitle, yearCountry, timeCategory, storyLine;
	private TextView IMDBRating, RTRating;
	private TextView directors, actors;
	
	// image
	private ImageView imgView;
	private TextView imgTitle;
//	private ImageButton showBtn;
	
	// music
	private ImageButton playBtn;
	private TextView musicTitle;

	public InformationActivity() {
		super();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Bundle extras = getIntent().getExtras();
		
		if (extras == null) finish();
		
		model = new InformationModel();
		dialogs = new InformationDialogs(this);


		switch(Constants.mediaType) {
		case IMAGE:
			setContentView(R.layout.activity_information_images);
			getImageElements();
			addImageActionListener();
			break;
		case FILM:
			setContentView(R.layout.activity_information_films);
			getFilmElements();
			addFilmActionListener();
			break;
		case MUSIC:
			setContentView(R.layout.activity_information_music);
			getMusicElements();
			addMusicActionListener();
			break;
		default:
		case ERROR:
			finish();
			break;
		}
	
		itemName = extras.getString("data");
		setView(itemName);
	}

	public void pinAccepted() {
		System.out.println("pinAccepted");
		dialogs.dismiss();
		BaseActivity.toast("Remote Access Acepted.");
		
		Constants.server.setRemoteAccess(true);

		switch(Constants.mediaType) {
		default: // ERROR
		case ERROR:
			System.err.println("Couldn't find " + Constants.mediaType);
			break;
		case FILM:
			launchFilm();
			break;
		case IMAGE:
			launchImage();
			break;
		case MUSIC:
			launchMusic();
			break;
			
		}
	}
	
	public void pinDenied() {
		System.out.println("pinDenied");
		dialogs.dismiss();
		BaseActivity.toast("Remote Access Denied.");
		
		Constants.server.setRemoteAccess(false);
	}
	
	// private
	// get elements
	private void getFilmElements() {
		cover = (ImageView) findViewById(R.id.cover);
		certificate = (ImageView) findViewById(R.id.certificate);
		filmTitle = (TextView) findViewById(R.id.title);
		timeCategory = (TextView) findViewById(R.id.timeCategory);
		yearCountry = (TextView) findViewById(R.id.yearCountry);
		storyLine = (TextView) findViewById(R.id.storyLine);
		IMDBRating = (TextView) findViewById(R.id.IMDBRating);
		RTRating = (TextView) findViewById(R.id.RTRating);
		directors = (TextView) findViewById(R.id.directors);
		actors = (TextView) findViewById(R.id.actors);
		streamBtn = (ImageButton) findViewById(R.id.streamBtn);
	}

	private void getImageElements() {
		imgView = (ImageView) findViewById(R.id.imgView);
		imgTitle = (TextView) findViewById(R.id.imgTitle);
//		showBtn = (ImageButton) findViewById(R.id.showBtn);
	}

	private void getMusicElements() {
		musicTitle = (TextView) findViewById(R.id.title);
		playBtn = (ImageButton) findViewById(R.id.playBtn);
	}
	
	// add action listeners
	private void addFilmActionListener() {
		streamBtn.setOnClickListener(onClick);
	}
	
	private void addImageActionListener() {
//		showBtn.setOnClickListener(onClick);
	}

	private void addMusicActionListener() {
		playBtn.setOnClickListener(onClick);
	}

	// other
	private void setView(String itemName) {
		MediaHandler media = Constants.media;

		switch (Constants.mediaType) {
		case IMAGE:
			Picture image = media.image().getByExactName(itemName);

			if (image != null) {
				imgView.setImageBitmap(image.getPoster());
				imgTitle.setText(image.getTitle());
			}
			break;
		case MUSIC:
			Song song = media.music().getByExactName(itemName);

			if (song != null) {
				musicTitle.setText(song.getTitle());
			}
			break;
		case FILM:
			Film film = media.film().getByExactName(itemName);

			if (film != null) {
				cover.setImageBitmap(film.getPoster());
				filmTitle.setText(film.getTitle());
				yearCountry.setText(String.valueOf(film.getYear()) + " | "
						+ film.getCountry());
				timeCategory.setText(film.getRunTime() + " min | "
						+ film.getGenre());
				storyLine.setText(film.getStoryLine());
				IMDBRating.setText("IMDB: " + getStars(film.getIMDBRating()));
				RTRating.setText("ROTT: " + getStars(film.getRTRating()));
				certificate
						.setImageResource(model.getBbfcResId(film.getRated()));
				directors.setText(arrayToString(film.getDirectors()));
				actors.setText(arrayToString(film.getActors()));
			}
			break;
		default:
			film = media.film().getByExactName(itemName);

			if (film != null) {
				cover.setImageBitmap(film.getPoster());
				filmTitle.setText(film.getTitle());
				yearCountry.setText(String.valueOf(film.getYear()) + " | "
						+ film.getCountry());
				timeCategory.setText(film.getRunTime() + " min | "
						+ film.getGenre());
				storyLine.setText(film.getStoryLine());
				IMDBRating.setText("IMDB: " + getStars(film.getIMDBRating()));
				RTRating.setText("ROTT: " + getStars(film.getRTRating()));
				certificate
						.setImageResource(model.getBbfcResId(film.getRated()));
				directors.setText(arrayToString(film.getDirectors()));
				actors.setText(arrayToString(film.getActors()));
			}
			break;
		}
	}
	
	// launch
	private void launchFilm() {
		String titleStr = filmTitle.getText().toString();
		Film film = Constants.media.film().getByExactName(titleStr);
		Constants.server.control().streamFilm(film.getFileName());
		
		BaseActivity.toast("Launching Film.");
		BaseActivity.launchActivity(RemoteActivity.class, itemName);
	}
	
	private void launchMusic() {
		String titleStr = musicTitle.getText().toString();
		Song music = Constants.media.music().getByExactName(titleStr);
		Constants.server.control().playSong(music.getFileName());
		
		BaseActivity.toast("Launching Music.");
		BaseActivity.launchActivity(RemoteActivity.class, itemName);
	}
	
	private void launchImage() {
		String titleStr = imgTitle.getText().toString();
		Picture image = Constants.media.image().getByExactName(titleStr);
		Constants.server.control().showImage(image.getFileName());
		
		BaseActivity.toast("Launching Image.");
		BaseActivity.launchActivity(RemoteActivity.class, itemName);
	}

	private String arrayToString(String[] array) {
		String string = "";
		
		if (array == null) {
			return string;
		}
		
		for (String str : array) {
			string += str + ", ";
		}

		return string.substring(0, string.length() - 2);
	}

	private String getStars(double rating) {
		String stars = "";

		for (int i = 0; i < rating; i++) {
			stars += "*";
		}

		return stars;
	}
	
	// listeners
	OnClickListener onClick = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			switch (arg0.getId()) {
			case R.id.streamBtn:
				if (!Constants.server.remoteAccess()) {
					dialogs.getPinDialog(inputOnClick);
				} else {
					launchFilm();
				}
				break;
			case R.id.playBtn:
				if (!Constants.server.remoteAccess()) {
					dialogs.getPinDialog(inputOnClick);
				} else {
					launchMusic();
				}
				break;
//			case R.id.showBtn:
//				if (!Constants.server.remoteAccess()) {
//					dialogs.getPinDialog(inputOnClick);
//				} else {
//					launchImage();
//				}
//				break;
			}
		}
	};

	// pin dialog
	DialogInterface.OnClickListener inputOnClick = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int id) {
			switch (id) {
			case -1: // Ok
				String pin = (String) dialogs.getEntry().getText().toString();

				if (!pin.equals("")) {
					Constants.server.control().beRemote(pin);
				}
				break;
			case -2: // Cancel
				break;
			}
		}
	};
}
