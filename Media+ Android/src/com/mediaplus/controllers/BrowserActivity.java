package com.mediaplus.controllers;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import com.mediaplus.R;
import com.mediaplus.main.BaseActivity;
import com.mediaplus.main.Constants;
import com.mediaplus.server.data.types.MediaType;

public class BrowserActivity extends BaseActivity {
	private ImageButton btnFilm, btnMusic, btnImage;
	
	public BrowserActivity() {
		super();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_browser);
		
		btnFilm = (ImageButton) findViewById(R.id.btnFilm);
		btnMusic = (ImageButton) findViewById(R.id.btnMusic);
		btnImage = (ImageButton) findViewById(R.id.btnImage);

		btnFilm.setOnClickListener(onItemClick);
		btnMusic.setOnClickListener(onItemClick);
		btnImage.setOnClickListener(onItemClick);
		
	}
	
	OnClickListener onItemClick = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			switch(arg0.getId()) {
			case R.id.btnFilm:
				Constants.mediaType = MediaType.FILM;
				break;
			case R.id.btnMusic:
				Constants.mediaType = MediaType.MUSIC;
				break;
			case R.id.btnImage:
				Constants.mediaType = MediaType.IMAGE;
				break;
			}
			launchActivity(WatchActivity.class);
		}
	};
}
