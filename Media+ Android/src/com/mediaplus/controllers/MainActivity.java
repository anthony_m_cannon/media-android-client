package com.mediaplus.controllers;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import com.mediaplus.R;
import com.mediaplus.dialogs.MainDialogs;
import com.mediaplus.main.BaseActivity;
import com.mediaplus.main.Constants;

public class MainActivity extends BaseActivity {
	private ImageButton remoteBtn, watchBtn;
	private MainDialogs dialogs;
	
	public MainActivity() {
		super();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		remoteBtn = (ImageButton) findViewById(R.id.remoteBtn);
		watchBtn = (ImageButton) findViewById(R.id.watchBtn);
		
		remoteBtn.setOnClickListener(onItemClick);
		watchBtn.setOnClickListener(onItemClick);
		
		dialogs = new MainDialogs(this);
		
		Bundle extras = getIntent().getExtras();
		
		if (extras != null) {
			String data = extras.getString("data");
			
			if (data.equals("splash")) {
				BaseActivity.launchActivity(ConnectionActivity.class);
			}
		}
	}
	
	// private
	OnClickListener onItemClick = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			switch(arg0.getId()) {
			case R.id.remoteBtn:
				if (Constants.server.isConnected()) {
					if (Constants.server.remoteAccess()) {
						launchActivity(RemoteActivity.class);
					} else {
						dialogs.getPinDialog(inputOnClick);
					}
				} else {
					dialogs.serverErrorDialog();
				}
				break;
			case R.id.watchBtn:
				if (Constants.server.isConnected()) {
					launchActivity(BrowserActivity.class);
				} else {
					dialogs.serverErrorDialog();
				}
				break;
			}
		}
	};
	
	// pin dialog
	DialogInterface.OnClickListener inputOnClick = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int id) {
			switch (id) {
			case -1: // Ok
				String pin = (String) dialogs.getEntry().getText().toString();
				
				if (!pin.equals("")) {
					Constants.server.control().beRemote(pin);
				}
				break;
			case -2: // Cancel
				break;
			}
		}
	};
	
	public void pinAccepted() {
		System.out.println("pinAccepted");
		dialogs.dismiss();
		BaseActivity.toast("Remote Access Acepted.");
		
		Constants.server.setRemoteAccess(true);
		launchActivity(RemoteActivity.class);
	}
	
	public void pinDenied() {
		System.out.println("pinDenied");
		dialogs.dismiss();
		BaseActivity.toast("Remote Access Denied.");
		
		Constants.server.setRemoteAccess(false);
	}
}
