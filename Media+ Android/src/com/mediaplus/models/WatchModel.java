package com.mediaplus.models;

import com.mediaplus.main.Constants;
import com.mediaplus.media.handlers.MediaHandler;

public class WatchModel extends BaseModel {
	private MediaHandler media;
	
	public WatchModel() {
		super();
		media = Constants.media;
	}
	
	public MediaHandler media() {
		return media;
	}
}
