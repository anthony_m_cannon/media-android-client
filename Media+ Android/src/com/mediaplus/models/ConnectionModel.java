package com.mediaplus.models;

import com.mediaplus.main.Constants;
import com.mediaplus.main.Preferences;

public class ConnectionModel extends BaseModel {
	private Preferences pref;
	
	public ConnectionModel() {
		pref = Constants.pref;
	}
	
	public String getIp() {
		return pref.getString("server-ip");
	}
	
	public void setIp(String value) {
		pref.saveString("server-ip", value);
	}
	
	public int getPort() {
		return pref.getInt("server-port");
	}
	
	public void setPort(int value) {
		pref.saveInt("server-port", value);
	}
}
