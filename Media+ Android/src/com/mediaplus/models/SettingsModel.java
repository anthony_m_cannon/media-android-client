package com.mediaplus.models;

import java.util.ArrayList;
import java.util.HashMap;

import com.mediaplus.main.Constants;
import com.mediaplus.main.Preferences;

public class SettingsModel extends BaseModel {
	private Preferences pref;
    private String[] groupList;
    private ArrayList<String[]> children;
    private ArrayList<ArrayList<String[]>> childrenList;
    private HashMap<String, ArrayList<String[]>> listCollection;
    
    public SettingsModel() {
		super();
    	pref = Constants.pref;
    	
        createGroupList();
        createChildList();
        createCollection();
	}
	
	public String[] groupList() {
		return groupList;
	}
	
	public HashMap<String, ArrayList<String[]>> listCollection() {
		return listCollection;
	}

	private void createGroupList() {
		groupList = new String[]{"Layout", "Server", "About"};
    }
	
	private void createChildList() {
		childrenList = new ArrayList<ArrayList<String[]>>();
		
		String[] c1 = new String[]{"Filter:", pref.getString("layout-filter")};
		
		children = new ArrayList<String[]>();
		children.add(c1);
		childrenList.add(children);
		
		c1 = new String[]{"Name:", pref.getString("server-name")};
		String[] c2 = new String[]{"IP:", pref.getString("server-ip")};
		String[] c3 = new String[]{"Port:", Integer.toString( pref.getInt("server-port") )};
		
		children = new ArrayList<String[]>();
		children.add(c1);
		children.add(c2);
		children.add(c3);
		childrenList.add(children);
		
		c1 = new String[]{"Device Name:", pref.getString("device-name")};
		c2 = new String[]{"Version:", "1"};
		c3 = new String[]{"Credits:", "Anthony"};
		
		children = new ArrayList<String[]>();
		children.add(c1);
		children.add(c2);
		children.add(c3);
		childrenList.add(children);
	}
 
    private void createCollection() {
    	listCollection = new HashMap<String, ArrayList<String[]>>();
    	
    	for (int i=0; i<groupList.length; i++) {
    		listCollection.put(groupList[i], childrenList.get(i));
    	}
    }

	public void setDeviceName(String name) {
		pref.saveString("device-name", name);
	}

	public void setServerName(String name) {
		pref.saveString("server-name", name);
	}

	public String getDeviceName() {
		return pref.getString("device-name");
	}

	public String getServerName() {
		return pref.getString("server-name");
	}
    
}
