package com.mediaplus.models;

import java.util.HashMap;
import java.util.Locale;

import com.mediaplus.R;

public class InformationModel extends BaseModel {
	private HashMap<String, Integer> bbfc;
	
	public InformationModel() {
		super();
		
		fillBbfc();
	}
	
	public int getBbfcResId(String rating) {
		if (rating != null ) {
			int image = R.drawable.bbfc_unknown;
			Object obj = bbfc.get(rating.toLowerCase(Locale.getDefault()));
			
			image = ((obj == null) ? image : Integer.parseInt(obj.toString()));
			
			return image;
		}
		
		return -1;
	}
	
	// private
	private void fillBbfc() {
		bbfc = new HashMap<String, Integer>();
		
		bbfc.put("u", R.drawable.bbfc_u);
		bbfc.put("pg", R.drawable.bbfc_pg);
		bbfc.put("12", R.drawable.bbfc_12);
		bbfc.put("12a", R.drawable.bbfc_12a);
		bbfc.put("15", R.drawable.bbfc_15);
		bbfc.put("18", R.drawable.bbfc_18);
		bbfc.put("r18", R.drawable.bbfc_r18);
		
		bbfc.put("g", R.drawable.mpaa_g);
		bbfc.put("nc17", R.drawable.mpaa_nc17);
		bbfc.put("pg13", R.drawable.mpaa_pg13);
		bbfc.put("r", R.drawable.mpaa_r);

		bbfc.put("N/A", R.drawable.pna);
		
	}
}
