package com.mediaplus.media;

import java.util.Arrays;

import android.graphics.Bitmap;

public class Film extends AVMedia {
	private double IMDBRating, RTRating, overAllRating;
	private String[] directors, actors;
	private String storyLine, rated, country;

	public Film(Bitmap image, String title, String extention) {
		super(image, title, extention);

		super.addInfo(0, 0, "N/A");

		this.IMDBRating = 0;
		this.RTRating = 0;
		overAllRating = workOutOverAllRating();
		this.directors = new String[] { "N/A" };
		this.actors = new String[] { "N/A" };
		this.storyLine = "N/A";
		this.rated = "N/A";
		this.country = "N/A";
	}

	public boolean needReGet() {
		if (varsAreDefault()) {
			return true;
		}

		return false;
	}

	public void addInfo(Bitmap poster, int year, int runTime, String genre,
			double iRating, double rRating, String[] directors,
			String[] actors, String storyLine, String rated, String country) {
		super.addInfo(year, runTime, genre);

		super.addInfo(poster);
		this.IMDBRating = iRating;
		this.RTRating = rRating;
		overAllRating = workOutOverAllRating();
		this.directors = directors;
		this.actors = actors;
		this.storyLine = storyLine;
		this.rated = rated;
		this.country = country;
	}

	// Gets
	public double getIMDBRating() {
		return IMDBRating;
	}

	public double getRTRating() {
		return RTRating;
	}

	public double getOverAllRating() {
		return overAllRating;
	}

	public String[] getDirectors() {
		return directors;
	}

	public String[] getActors() {
		return actors;
	}

	public String getStoryLine() {
		return storyLine;
	}

	public String getCountry() {
		return country;
	}

	public String getRated() {
		return rated;
	}

	@Override
	public String toString() {
		return "Film [IMDBRating=" + IMDBRating + ", RTRating=" + RTRating
				+ ", overAllRating=" + overAllRating + ", directors="
				+ Arrays.toString(directors) + ", actors="
				+ Arrays.toString(actors) + ", storyLine=" + storyLine
				+ ", rated=" + rated + ", country=" + country
				+ "z, toString()=" + super.toString() + "]";
	}

	// Private functions
	private double workOutOverAllRating() {
		return (IMDBRating + RTRating) / 2;
	}

	private boolean varsAreDefault() {
		// null checkers
		if (directors == null || actors == null || storyLine == null
				|| rated == null || country == null) {
			return true;
		}
		
		if (((Object) IMDBRating) == null || ((Object) RTRating) == null || ((Object) overAllRating) == null) {
			return true;
		}

		if (directors == null || actors == null) {
			return true;
		}
		
		// default checker
		if (IMDBRating == 0 || RTRating == 0 || overAllRating == 0) {
			return true;
		}

		if (directors[0].equals("N/A") || actors[0].equals("N/A")) {
			return true;
		}

		if (storyLine.equals("N/A") || rated.equals("N/A")
				|| country.equals("N/A")) {
			return true;
		}

		return false;
	}

}
