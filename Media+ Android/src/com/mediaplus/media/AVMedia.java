package com.mediaplus.media;

import android.graphics.Bitmap;


public class AVMedia extends Media {
	private String genre;
	private int year, runTime;
	
	public AVMedia(Bitmap image, String title, String extention) {
		super(image, title, extention);
	}
	
	protected void addInfo(int year, int runTime, String genre) {
		this.year = year;
		this.runTime = runTime;
		this.genre = genre;
	}
	
	// Gets
	public int getYear() {
		return year;
	}
	
	public int getRunTime() {
		return runTime;
	}
	
	public String getGenre() {
		return genre;
	}

	@Override
	public String toString() {
		return "AVMedia [genre=" + genre + ", year=" + year + ", runTime="
				+ runTime + ", toString()=" + super.toString() + "]";
	}

	
}
