package com.mediaplus.media;

import java.util.Arrays;

import android.graphics.Bitmap;

public class Song extends AVMedia {
	private String artist, producer, album;
	private String[] featuring;
	
	public Song(Bitmap image, String title, String extention) {
		super(image, title, extention);
		
		super.addInfo(0, 0, "N/A");
		this.artist = "N/A";
		this.producer = "N/A";
		this.album = "N/A";
		this.featuring = new String[] {"N/A"};
	}
	
	public void addInfo(Bitmap poster, int year, int runTime, String genre, String artist, String producer, String album, String[] featuring) {
		super.addInfo(year, runTime, genre);
		
		super.addInfo(poster);
		this.artist = artist;
		this.producer = producer;
		this.album = album;
		this.featuring = featuring;
	}
	
	// Gets
	public String getArtist() {
		return artist;
	}
	
	public String getProducer() {
		return producer;
	}
	
	public String getAlbum() {
		return album;
	}
	
	public String[] getFeaturing() {
		return featuring;
	}

	@Override
	public String toString() {
		return "Song [artist=" + artist + ", producer=" + producer + ", album="
				+ album + ", featuring=" + Arrays.toString(featuring)
				+ ", toString()=" + super.toString() + "]";
	}
	
	

}
