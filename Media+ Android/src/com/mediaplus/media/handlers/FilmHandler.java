package com.mediaplus.media.handlers;

import java.util.ArrayList;
import java.util.Locale;

import android.graphics.Bitmap;

import com.mediaplus.media.Film;
import com.mediaplus.media.interfaces.AVMediaInterface;

public class FilmHandler implements AVMediaInterface {
	private ArrayList<Film> films;
	private Bitmap pna;
	
	public FilmHandler(Bitmap pna) {
		films = new ArrayList<Film>();
		
		this.pna = pna;
	}
	
	public void add(Film film) {
		films.add(film);
	}
	
	public ArrayList<String> getCorruptedFilms() {
		ArrayList<String> reGetFilms = new ArrayList<String>();
		
		for (Film film : films) {
			if (film.needReGet()) {
				reGetFilms.add(film.getFileName());
			}
		}
		
		return reGetFilms;
	}

	public void newFilm(String title, String extention) {
		Film film = new Film(pna, title, extention);
		if (!filmExists(film)) {
			films.add(film);
		}
	}
	
	public void addFilmInfo(Bitmap poster, String title, int year, int runTime, String genre, double IMDBRating, double RTRating, String[] directors, String[] actors, String storyLine, String rated, String country) {
		for (Film film : films) {
			if (film.getTitle().equals(title)) {
				film.addInfo(poster, year, runTime, genre, IMDBRating, RTRating, directors, actors, storyLine, rated, country);
				break;
			}
		}
	}
	
	@Override
	public ArrayList<String> getGenres() {
		ArrayList<String> genres = new ArrayList<String>();
		
		for (Film film : this.films) {
			String genre = film.getGenre();
			if (!genres.contains(genre)) {
				genres.add(genre);
			}
		}
		
		return genres;
	}
	
	@Override
	public ArrayList<Integer> getYears() {
		ArrayList<Integer> years = new ArrayList<Integer>();
		
		for (Film film : this.films) {
			int year = film.getYear();
			if (!years.contains(year)) {
				years.add(year);
			}
		}
		
		return years;
	}

	@Override
	public ArrayList<Film> getAll() {
		return films;
	}
	
	@Override
	public ArrayList<Film> getByGenre(String genre) {
		ArrayList<Film> films = new ArrayList<Film>();
		
		for (Film film : this.films) {
			if (film.getGenre().equals(genre)) {
				films.add(film);
			}
		}
		
		return films;
	}

	@Override
	public ArrayList<Film> getByYear(int year) {
		ArrayList<Film> films = new ArrayList<Film>();
		
		for (Film film : this.films) {
			if (film.getYear() == year) {
				films.add(film);
			}
		}
		
		return films;
	}

	@Override
	public Film getByExactName(String name) {
		for (Film film : films) {
			if (film.getTitle().equalsIgnoreCase(name)) {
				return film;
			}
		}
		return null;
	}
	
	@Override
	public Film getByPartialName(String name) {
		ArrayList<Film> tempFilms = new ArrayList<Film>(films);
		name = name.toLowerCase(Locale.getDefault());
		for (Film film : tempFilms) {
			String title = film.getTitle().toLowerCase(Locale.getDefault());
			if (title.contains(name)) {
				return film;
			}
		}
		return null;
	}

	@Override
	public int getAmount() {
		return films.size();
	}
	
	// private
	private boolean filmExists(Film f) {
		for (Film film : films) {
			if (film.getTitle().equals(f.getTitle())) {
				return true;
			}
		}
		
		return false;
	}
}
