package com.mediaplus.media.handlers;

import java.util.ArrayList;
import java.util.Locale;

import android.graphics.Bitmap;

import com.mediaplus.media.Song;
import com.mediaplus.media.interfaces.AVMediaInterface;

public class SongHandler implements AVMediaInterface {
	private final ArrayList<Song> songs;
	private Bitmap pna;
	
	public SongHandler(Bitmap pna) {
		songs = new ArrayList<Song>();
		
		this.pna = pna;
	}
	
	public void add(Song song) {
		songs.add(song);
	}
	
	public void newSong(String title, String extention) {
		Song song = new Song(pna, title, extention);
		if (!songExists(song)) {
			songs.add(song);
		}
	}
	
	public void addFilmInfo(Bitmap poster, String title, int year, int runTime, String genre, String artist, String producer, String album, String[] featuring) {
		for (Song song : songs) {
			if (song.getTitle().equals(title)) {
				song.addInfo(poster, year, runTime, genre, artist, producer, album, featuring);
				break;
			}
		}
	}
	
	@Override
	public ArrayList<String> getGenres() {
		ArrayList<String> genres = new ArrayList<String>();
		
		for (Song song : this.songs) {
			String genre = song.getGenre();
			if (!genres.contains(genre)) {
				genres.add(genre);
			}
		}
		
		return genres;
	}
	
	@Override
	public ArrayList<Integer> getYears() {
		ArrayList<Integer> years = new ArrayList<Integer>();
		
		for (Song song : this.songs) {
			int year = song.getYear();
			if (!years.contains(year)) {
				years.add(year);
			}
		}
		
		return years;
	}

	@Override
	public ArrayList<Song> getAll() {
		return songs;
	}

	@Override
	public ArrayList<Song> getByGenre(String genre) {
		ArrayList<Song> songs = new ArrayList<Song>();
		
		for (Song song : this.songs) {
			if (song.getGenre().equals(genre)) {
				songs.add(song);
			}
		}
		
		return songs;
	}

	@Override
	public ArrayList<Song> getByYear(int year) {
		ArrayList<Song> songs = new ArrayList<Song>();
		
		for (Song song : this.songs) {
			if (song.getYear() == year) {
				songs.add(song);
			}
		}
		
		return songs;
	}

	@Override
	public Song getByExactName(String name) {
		for (Song song : songs) {
			if (song.getTitle().equalsIgnoreCase(name)) {
				return song;
			}
		}
		return null;
	}

	@Override
	public synchronized Song getByPartialName(String name) {
		ArrayList<Song> tempSongs = new ArrayList<Song>(songs);
		name = name.toLowerCase(Locale.getDefault());
		for (Song song : tempSongs) {
			String title = song.getTitle().toLowerCase(Locale.getDefault());
			if (title.contains(name)) {
				return song;
			}
		}
		return null;
	}

	@Override
	public int getAmount() {
		return songs.size();
	}
	
	// private
	private boolean songExists(Song s) {
		for (Song song : songs) {
			if (song.getTitle().equals(s.getTitle())) {
				return true;
			}
		}
		
		return false;
	}
	
}
