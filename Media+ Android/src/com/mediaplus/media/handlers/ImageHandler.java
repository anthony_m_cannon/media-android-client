package com.mediaplus.media.handlers;

import java.util.ArrayList;
import java.util.Locale;

import android.graphics.Bitmap;

import com.mediaplus.media.Picture;
import com.mediaplus.media.interfaces.MediaInterface;

public class ImageHandler implements MediaInterface {
	private ArrayList<Picture> images;
	private Bitmap pna;

	public ImageHandler(Bitmap pna) {
		images = new ArrayList<Picture>();

		this.pna = pna;
	}
	
	public void add(Picture image) {
		images.add(image);
	}

	public void newImage(String title, String extention) {
		Picture image = new Picture(pna, title, extention);
		if (!imageExists(image)) {
			images.add(image);
		}
	}

	public void newImage(Bitmap bit, String title, String extention) {
		Picture image = new Picture(bit, title, extention);
		if (!imageExists(image)) {
			images.add(image);
		}
	}

	@Override
	public ArrayList<Picture> getAll() {
		return images;
	}

	@Override
	public Picture getByExactName(String name) {
		for (Picture image : images) {
			if (image.getTitle().equalsIgnoreCase(name)) {
				return image;
			}
		}
		return null;
	}
	
	@Override
	public Picture getByPartialName(String name) {
		ArrayList<Picture> tempImages = new ArrayList<Picture>(images);
		name = name.toLowerCase(Locale.getDefault());
		for (Picture image : tempImages) {
			String title = image.getTitle().toLowerCase(Locale.getDefault());
			if (title.contains(name)) {
				return image;
			}
		}
		return null;
	}

	@Override
	public int getAmount() {
		return images.size();
	}

	// private
	private boolean imageExists(Picture i) {
		for (Picture image : images) {
			if (image.getTitle().equals(i.getTitle())) {
				return true;
			}
		}

		return false;
	}
	
}
