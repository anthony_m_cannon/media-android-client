package com.mediaplus.media.handlers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.mediaplus.R;
import com.mediaplus.main.Constants;
import com.mediaplus.server.data.types.MediaType;


public class MediaHandler {
	private final FilmHandler film;
	private final SongHandler music;
	private final ImageHandler image;

	private Bitmap pna;
	
	public MediaHandler() {
		pna = BitmapFactory.decodeResource(Constants.activity.getResources(), R.drawable.pna);
		
		film = new FilmHandler(pna);
		music = new SongHandler(pna);
		image = new ImageHandler(pna);
	}
	
	public MediaType getMediaTypeByTitle(String title) {
		if (film.getByExactName(title) != null) {
			return MediaType.FILM;
		} else if (music.getByExactName(title) != null) {
			return MediaType.MUSIC;
		} else if (image.getByExactName(title) != null) {
			return MediaType.IMAGE;
		}
		
		return MediaType.ERROR;
	}
	
	public Bitmap getPna() {
		return pna;
	}
	
	public FilmHandler film() {
		return film;
	}
	
	public SongHandler music() {
		return music;
	}
	
	public ImageHandler image() {
		return image;
	}
}
