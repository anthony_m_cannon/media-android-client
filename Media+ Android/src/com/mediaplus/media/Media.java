package com.mediaplus.media;

import android.graphics.Bitmap;


public class Media {
	private Bitmap poster;
	private final String title;
	private final String extention;
	
	public Media(Bitmap image, String title, String extention) {
		this.poster = image;
		this.title = title;
		this.extention = extention;
	}
	
	public void addInfo(Bitmap poster) {
		this.poster = poster;
	}
	
	// Gets
	public Bitmap getPoster() {
		return poster;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getExtention() {
		return extention;
	}
	
	public String getFileName() {
		return getTitle() + "." + extention;
	}

	@Override
	public String toString() {
		return "Media [title=" + title + ", extention=" + extention + "]";
	}
}
