package com.mediaplus.media.interfaces;

import java.util.ArrayList;

public interface MediaInterface {
	public ArrayList<?> getAll();
	public Object getByExactName(String name);
	public Object getByPartialName(String name);
	public int getAmount();
}
