package com.mediaplus.media.interfaces;

import java.util.ArrayList;

public interface AVMediaInterface extends MediaInterface {
	
	public ArrayList<Integer> getYears();
	public ArrayList<String> getGenres();
	
	public ArrayList<?> getByGenre(String genre);
	public ArrayList<?> getByYear(int year);
	
}
