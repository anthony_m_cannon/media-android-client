package com.mediaplus.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServerValidator {
	private Pattern ipPattern;
	private Matcher ipMatcher;

	private static final String IPADDRESS_PATTERN = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

	public ServerValidator() {
		ipPattern = Pattern.compile(IPADDRESS_PATTERN);
	}

	/**
	 * Validate ip address with regular expression
	 * 
	 * @param ip
	 *            ip address for validation
	 * @return true valid ip address, false invalid ip address
	 */
	public boolean ipValidate(final String ip) {
		ipMatcher = ipPattern.matcher(ip);
		
		return ipMatcher.matches();
	}
	
	public boolean portValidate(final int port) {
		return port < 49151;
	}
}