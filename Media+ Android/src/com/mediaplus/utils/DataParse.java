package com.mediaplus.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.mediaplus.R;
import com.mediaplus.filter.FilterHandler;
import com.mediaplus.main.Constants;
import com.mediaplus.media.Film;
import com.mediaplus.media.Picture;
import com.mediaplus.media.Song;
import com.mediaplus.media.handlers.MediaHandler;
import com.mediaplus.server.data.types.MediaType;

public class DataParse {

	public boolean tryGetData(String data) {
		if (data.contains("title") || data.contains("fileName")) {
			if (data.contains("�")) {
				String[] bits = data.split("�");
				HashMap<String, String> map = new HashMap<String, String>();
				
				for (String bit : bits) {
					if (bit.contains("=")) {
						String[] temp = bit.split("=");
						if (temp.length == 2) {
							map.put(temp[0], temp[1]);
						}
					}
				}
				
				return readyInfoForPopulation(map);
			}
		} else {
			if (!data.contains("�") && !data.contains("=")) {
				FilterHandler filter = new FilterHandler();
				
				MediaType mt = filter.accept(data);
				switch(mt) {
				default: // error
				case ERROR:
					return false;
				case FILM:
					int point = data.lastIndexOf('.');
					if (point == -1) return false;
					
					String title = data.substring(point);
					
					if (Constants.media.film().getByPartialName(title) != null) {
						Bitmap poster = Constants.media.getPna();
						String extention = data.substring(point + 1);
						
						Film film = new Film(poster, title, extention);
						Constants.media.film().add(film);
					}
				case IMAGE:
					point = data.lastIndexOf('.');
					if (point == -1) return false;
					
					title = data.substring(point);
					
					if (Constants.media.image().getByPartialName(title) != null) {
						Bitmap poster = Constants.media.getPna();
						
						String extention = data.substring(point + 1);
						
						Picture image = new Picture(poster, title, extention);
						Constants.media.image().add(image);
					}
				case MUSIC:
					point = data.lastIndexOf('.');
					if (point == -1) return false;
					
					title = data.substring(point);
					
					if (Constants.media.music().getByPartialName(title) != null) {
						Bitmap poster = Constants.media.getPna();
						String extention = data.substring(point + 1);
						
						Song song = new Song(poster, title, extention);
						Constants.media.music().add(song);
					}
				}
			}
		}
		
		return false;
	}
	
	// private
	private boolean readyInfoForPopulation(HashMap<String, String> map) {
		String title = null;
		
		if (map.get("title") == null) {
			if (map.get("fileName") != null) {
				String fileName = map.get("fileName");
				title = fileName.substring(0, fileName.lastIndexOf("."));
			}
		} else {
			title = map.get("title");
		}
		
		if (title != null) {
			return populateMedia(title, map);
		}
		
		return false;
	}
	
	private boolean populateMedia(String title, HashMap<String, String> map) {
		Object obj = Constants.media.getMediaTypeByTitle(title);

		MediaHandler handler = Constants.media;
		
		MediaType mt = (MediaType) obj;
		switch(mt) {
		default: // error
		case ERROR: // insert new media
			String fileName = map.get("fileName");
			if (fileName != null) {
				FilterHandler filter = new FilterHandler();
				mt = filter.accept(fileName);
				
				switch(mt) {
				default: // error
				case ERROR:
					return false;
				case FILM:
					return newFilm(handler, title, fileName, map);
				case IMAGE:
					return newImage(handler, title, fileName, map);
				case MUSIC:
					return newMusic(handler, title, fileName, map);
				}
			}
			break;
		case FILM:
			return populateFilm(handler, title, map);
		case IMAGE:
			return populateImage(handler, title, map);
		case MUSIC:
			return populateMusic(handler, title, map);
		}
		
		return false;
	}
	
	private boolean newMusic(MediaHandler handler, String title, String fileName, HashMap<String, String> map) {
		Bitmap poster = (Bitmap) ((map.get("poster") == null) ? handler.getPna() : getBitmapFromURL(map.get("poster")));
		String extention = fileName.substring(0, fileName.lastIndexOf('.') + 1);
		
		Song song = new Song(poster, title, extention);
		handler.music().add(song);
		
		return true;
	}

	private boolean newImage(MediaHandler handler, String title, String fileName, HashMap<String, String> map) {
		Bitmap poster = (Bitmap) ((map.get("poster") == null) ? handler.getPna() : getBitmapFromURL(map.get("poster")));
		String extention = fileName.substring(0, fileName.lastIndexOf('.') + 1);
		
		Picture image = new Picture(poster, title, extention);
		Constants.media.image().add(image);
		
		return true;
	}

	private boolean newFilm(MediaHandler handler, String title, String fileName, HashMap<String, String> map) {
		Bitmap poster = (Bitmap) ((map.get("poster") == null) ? handler.getPna() : getBitmapFromURL(map.get("poster")));
		
		String genre = (String) ((map.get("genre") == null) ? "N/A" : map.get("genre"));
		String storyLine = (String) ((map.get("storyLine") == null) ? "N/A" : map.get("storyLine"));
		String rated = (String) ((map.get("rated") == null) ? "N/A" : map.get("rated"));
		String country = (String) ((map.get("country") == null) ? "N/A" : map.get("country"));
		String extention = fileName.substring(0, fileName.lastIndexOf('.') + 1);
		
		int year = 0;
		String yearStr = map.get("year");
		if (yearStr != null) {
			if (isNumber(yearStr)) {
				year = Integer.parseInt(yearStr);
			}
		}
		
		int runTime = 0;
		String runTimeStr = map.get("runTime");
		if (runTimeStr != null) {
			if (isNumber(runTimeStr)) {
				runTime = Integer.parseInt(runTimeStr);
			}
		}
		
		double iRating = 0;
		String imdbRatingStr = map.get("imdbRating");
		if (imdbRatingStr != null) {
			if (isNumber(imdbRatingStr)) {
				iRating = Double.parseDouble(imdbRatingStr);
			}
		}
		
		double rRating = 0;
		String rRatingStr = map.get("tomatoRating");
		if (rRatingStr != null) {
			if (isNumber(rRatingStr)) {
				iRating = Double.parseDouble(rRatingStr);
			}
		}
		
		String directorsStr = (String) ((map.get("director") == null) ? "N/A" : map.get("director"));
		String actorsStr = (String) ((map.get("actors") == null) ? "N/A" : map.get("actors"));
		
		String[] directors;
		if (directorsStr.contains(",")) {
			directors = directorsStr.split(",");
		} else {
			directors = new String[] {directorsStr};
		}
		
		String[] actors;
		if (actorsStr.contains(",")) {
			actors = actorsStr.split(",");
		} else {
			actors = new String[] {actorsStr};
		}
		
		Film film = new Film(poster, title, extention);
		film.addInfo(poster, year, runTime, genre, iRating, rRating, directors, actors, storyLine, rated, country);
		
		handler.film().add(film);
		return true;
	}

	private boolean populateMusic(MediaHandler handler, String title,
			HashMap<String, String> map) {
		
//		Song song = handler.music().getByName(title);
		return true;
	}

	private boolean populateImage(MediaHandler handler, String title,
			HashMap<String, String> map) {
		
//		Picture image = handler.image().getByName(title);
		return true;
	}

	private boolean populateFilm(MediaHandler handler, String title, HashMap<String, String> map) {
		Film film = handler.film().getByExactName(title);
		
		Bitmap poster = (Bitmap) ((map.get("poster") == null) ? film.getPoster() : getBitmapFromURL(map.get("poster")));
		
		String genre = (String) ((map.get("genre") == null) ? film.getGenre() : map.get("genre"));
		String storyLine = (String) ((map.get("storyLine") == null) ? film.getStoryLine() : map.get("storyLine"));
		String rated = (String) ((map.get("rated") == null) ? film.getRated() : map.get("rated"));
		String country = (String) ((map.get("country") == null) ? film.getCountry() : map.get("country"));
		
		int year = 0;
		String yearStr = map.get("year");
		if (yearStr != null) {
			if (isNumber(yearStr)) {
				year = film.getYear();
			}
		}
		
		int runTime = 0;
		String runTimeStr = map.get("runTime");
		if (runTimeStr != null) {
			if (isNumber(runTimeStr)) {
				runTime = film.getRunTime();
			}
		}
		
		double iRating = 0;
		String imdbRatingStr = map.get("imdbRating");
		if (imdbRatingStr != null) {
			if (isNumber(imdbRatingStr)) {
				iRating = film.getIMDBRating();
			}
		}
		
		double rRating = 0;
		String rRatingStr = map.get("tomatoRating");
		if (rRatingStr != null) {
			if (isNumber(rRatingStr)) {
				iRating = film.getRTRating();
			}
		}
		
		String directorsStr = (String) ((map.get("director") == null) ? Arrays.toString(film.getDirectors()) : map.get("director"));
		String actorsStr = (String) ((map.get("actors") == null) ? Arrays.toString(film.getActors()) : map.get("actors"));
		
		directorsStr = directorsStr.replace("[", "");
		directorsStr = directorsStr.replace("]", "");
		
		actorsStr = actorsStr.replace("[", "");
		actorsStr = actorsStr.replace("]", "");
		
		String[] directors;
		if (directorsStr.contains(",")) {
			directors = directorsStr.split(",");
		} else {
			directors = new String[] {directorsStr};
		}
		
		String[] actors;
		if (actorsStr.contains(",")) {
			actors = actorsStr.split(",");
		} else {
			actors = new String[] {actorsStr};
		}
		
		film.addInfo(poster, year, runTime, genre, iRating, rRating, directors, actors, storyLine, rated, country);
		
		return true;
	}
	
	private boolean isNumber(String string) {
		for (int i=0; i<string.length(); i++) {
			char c = string.charAt(i);
			if (!Character.isDigit(c)) {
				return false;
			}
		}
		
		return true;
	}

	private Bitmap getBitmapFromURL(String link) {
		try {
			URL url = new URL(link);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);

			return myBitmap;

		} catch (IOException e) {
			System.err.println(e.getMessage());
			
			Bitmap image = BitmapFactory.decodeResource(
					Constants.activity.getResources(), R.drawable.pna);
			
			return image;
		}

	}
}
