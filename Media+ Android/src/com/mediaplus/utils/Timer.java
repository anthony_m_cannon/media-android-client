package com.mediaplus.utils;

public class Timer implements Runnable {
	private int time;
	
	public Timer() {
		this.time = 0;
	}
	
	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				return;
			}
			
			time++;
		}
	}
	
	public int getTime() {
		return time;
	}
}
