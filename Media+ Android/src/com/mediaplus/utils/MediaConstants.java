package com.mediaplus.utils;

public class MediaConstants {
	public static boolean playing;
	public static boolean muted;
	
	public MediaConstants() {
		playing = false;
		muted = false;
	}
}
